#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

const cl_uint ITERATIONS = 1024;
const cl_ulong MILLION = 1000000;

const std::string output_filename = "../07_broadcasting_parallel_access.txt";
std::ofstream output(output_filename);

cl::CommandQueue queue;
cl::Context context;
cl::Program program;

int platform_number = 0;
int device_number = 0;
unsigned int device_type = CL_DEVICE_TYPE_GPU;

/* This microbenchmark is based on the 2018 paper "Benchmarking the GPU memory at the warp level",
 * which introduces microbenchmarks measuring whether the underlying hardware memory supports
 * broadcasting (multiple work-items accessing a single item, MTSD) and parallel accessing
 * (multiple work-items accessing multiple distinct items, MTMD). Intuitively, we would guess
 * GPUs would generally support both and CPUs would generally not support either. We test this
 * on the three types of OpenCL memory - global, constant, and local (shared in CUDA terminology).
 * Output is a list of tuples of (MEMORY_TYPE, TEST_NUMBER, THREADS_NUMBER, BROADCASTING_DEGREE, ms/ACCESS, CLOCKS/ACCESS).
 * By comparing the outputs, we can conclude whether broadcasting and/or parallel accessing are supported by the HW.
*/

/* Allocates a contiguous access pattern. */
cl_uint* initialize_access_01(const cl_ulong N) {
	cl_uint* a = new cl_uint[N];
	for (cl_uint i = 0; i < N; i++) {
		a[i] = i;
	}
	return a;
}

/* Allocates an array a of size N whose values are randomized. Odd algorithm used in Fang's paper. */
cl_uint* initialize_access_02(const cl_ulong N) {
	cl_uint* a = new cl_uint[N];
	cl_uint p[32];

	/* Initializes the first 32 elements of p - they are [0..31]. */
	for (cl_uint i = 0; i < 32; i++) {
		p[i] = i;
	}
	for (cl_uint i = 0; i < N; ) {
		for (cl_uint j = 0; j < 32; j++) {
			cl_uint jj = rand() % (32 - j); // select random element in [0..31], then [0..30], ... then [0]
			a[i + j] = p[jj]; // the next a is this random element
			for (cl_uint k = jj; k < 32 - j; k++) { // move elements of p down one
				p[k] = p[k + 1];
			}
		}
		/* Regenerate elements of p by making them equal to the most recent 32 elements of a. 
		 * The next 32 elements of a are equal to the previous 32, incremented by 32.
		 */
		for (cl_uint j = 0; j < 32; j++) {
			p[j] = a[i + j];
			a[i + j] = a[i + j] + i; //increments a[i*32.. i*32+31] by i*32
		}
		i += 32;
	}
	return a;
}

// Create a NxN matrix whose columns are mixed up. Instead of the method in Fang's paper,
// uses Fisher-Yates shuffle per column.
cl_uint* initialize_access_03(const cl_ulong N) {

	cl_uint* a = new cl_uint[N * N];

	for (cl_uint i = 0; i < N * N; i++) {
		a[i] = i;
	}
	for (cl_uint j = 0; j < N; j++) {
		for (cl_uint i = 0; i < N; i++) {
			int pos = rand() % (N - i);
			cl_uint tmp = a[pos * N + j];
			a[pos * N + j] = a[(N - i - 1) * N + j];
			a[(N - i - 1) * N + j] = tmp;
		}
	}
	return a;
}


void run_test(cl::Context& context, cl::CommandQueue& queue, cl::Kernel& kernel,
	const cl_uint test_number, const cl_uint total_threads, 
	const cl_uint threads_in_warp, const cl_uint broadcasting_degree, bool test_local = false) {

	cl_uint* in1, * in2;
	const cl_ulong N = total_threads; // memory size matches the number of threads or the number of threads squared
	cl_ulong* elapsed_time = new cl_ulong[N];
	if (test_number == 1) {
		in1 = initialize_access_01(N);
		in2 = initialize_access_01(N);
	}
	else if (test_number == 2) {
		in1 = initialize_access_02(N);
		in2 = initialize_access_02(N);
	}
	else if (test_number == 3) {
		in1 = initialize_access_03(N);
		in2 = initialize_access_03(N);
	}

	// Test 3 creates a matrix, hence we need N*N space.
	const cl_ulong allocate_number = (test_number == 3) ? N * N : N;

	// Allocate buffers and write to them.
	cl::Buffer bufferIn1 = cl::Buffer(context,
		CL_MEM_READ_ONLY, allocate_number * sizeof(cl_uint));
	cl::Buffer bufferIn2 = cl::Buffer(context,
		CL_MEM_READ_ONLY, allocate_number * sizeof(cl_uint));
	cl::Buffer bufferClock = cl::Buffer(context,
		CL_MEM_WRITE_ONLY, N * sizeof(cl_ulong));
	queue.enqueueWriteBuffer(bufferIn1, CL_TRUE, 0,
		allocate_number * sizeof(cl_uint), in1);
	queue.enqueueWriteBuffer(bufferIn2, CL_TRUE, 0,
		allocate_number * sizeof(cl_uint), in2);

	kernel.setArg(0, bufferIn1);
	kernel.setArg(1, bufferIn2);
	kernel.setArg(2, bufferClock);
	kernel.setArg(3, broadcasting_degree);
	kernel.setArg(4, ITERATIONS);
	if (test_local) {
		// Allocate local memory dynamically.
		kernel.setArg(5, allocate_number * sizeof(cl_uint), NULL);
		kernel.setArg(6, allocate_number * sizeof(cl_uint), NULL);
	}

	cl::NDRange global(total_threads);
	cl::NDRange local(std::min(total_threads, threads_in_warp));

	cl::Event time_event;
	queue.enqueueNDRangeKernel(kernel,
		cl::NullRange, global, local, nullptr, &time_event);
	time_event.wait();
	double elapsed = (double)(time_event.getProfilingInfo<CL_PROFILING_COMMAND_END>() - 
							  time_event.getProfilingInfo<CL_PROFILING_COMMAND_START>());
	elapsed /= MILLION; // translate to ms	
	queue.enqueueReadBuffer(bufferClock, CL_TRUE, 0,
		N * sizeof(cl_ulong), elapsed_time);

	double clk = elapsed_time[0];
	clk /= (ITERATIONS * 2);
	output << elapsed << ", " << clk;

	delete[] in1;
	delete[] in2;
}

void test_global_memory(std::vector<cl_uint>& accesses, std::vector<cl_uint>& threads,
std::vector<cl_uint>& tests) {
	cl_uint warp_size = (cl_uint)kernel_global.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>
																(devices[device_number]);
	cl::Kernel kernel_global(program, "access_global");
	for (auto test_number : tests) {
		for (auto thread_number : threads) {
			for (auto broadcasting_degree : accesses) {
				output << "(GLOBAL, TEST" << test_number << ", " << thread_number << ", " << broadcasting_degree << ", ";
				run_test(context, queue, kernel_global, test_number, thread_number, warp_size, broadcasting_degree);
				output << ")" << std::endl;
			}
		}
	}
}

void test_local_memory(std::vector<cl_uint>& accesses, std::vector<cl_uint>& threads,
std::vector<cl_uint>& tests) {
	cl_uint warp_size = (cl_uint)kernel_global.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>
																(devices[device_number]);
	cl::Kernel kernel_local(program, "access_local");
	cl_ulong local_memory_size = devices[device_number].getInfo<CL_DEVICE_LOCAL_MEM_SIZE>();
	for (auto test_number : tests) {
		for (auto thread_number : threads) {
			for (auto broadcasting_degree : accesses) {
				// have to check if test #3 space requirements are higher than the
				// capacity of local memory - multiplied by 2 as there are two arrays
				if (test_number != 3 || (cl_ulong)thread_number * thread_number * 2* sizeof(cl_int) < local_memory_size) {
					output << "(LOCAL, TEST" << test_number << ", " << thread_number << ", " << broadcasting_degree << ", ";
					run_test(context, queue, kernel_local, test_number, thread_number, warp_size, broadcasting_degree, true);
					output << ")" << std::endl;
				}
			}
		}
	}
}

void test_constant_memory(std::vector<cl_uint>& accesses, std::vector<cl_uint>& threads,
std::vector<cl_uint>& tests) {

	/* According to the standard, the minimum constant memory buffer size is 64 KiB. Our test reaches 4 MiB in one dimension,
	which is 2 KiB (4 B * 512 threads, then squared for test #3) per buffer (there are two buffers), so we check the size only for test #3. */		 
	cl_ulong constant_memory_size = devices[device_number].getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>();
	cl::Kernel kernel_constant(program, "access_constant");
	for (auto test_number : tests) {
		for (auto thread_number : threads) {
			for (auto broadcasting_degree : accesses) {
				// have to check if test #3 overtakes the constant memory - not multiplied by two here
				if (test_number != 3 || (cl_ulong)thread_number * thread_number *  sizeof(cl_int) < constant_memory_size) {
					output << "(CONSTANT, TEST" << test_number << ", " << thread_number << ", " << broadcasting_degree << ", ";
					run_test(context, queue, kernel_constant, test_number, thread_number, warp_size, broadcasting_degree);
					output << ")" << std::endl;
				}
			}
		}
	}
}

int main() {
	try {
		std::vector<cl::Platform>platforms;
		cl::Platform::get(&platforms);

		std::vector<cl::Device>devices;
		platforms[platform_number].getDevices(device_type, &devices);
		
		context = cl::Context(devices);
		queue =	cl::CommandQueue(context, devices[device_number], CL_QUEUE_PROFILING_ENABLE);

		std::ifstream sourceFile("kernel_07.cl");
		std::string sourceCode(
			std::istreambuf_iterator<char>(sourceFile),
			(std::istreambuf_iterator<char>()));
		cl::Program::Sources source(1,
			std::make_pair(sourceCode.c_str(),
				sourceCode.length() + 1));
		program = cl::Program(context, source);
		program.build(devices, "-cl-opt-disable");

		cl::Kernel kernel_local(program, "access_local");
		cl::Kernel kernel_constant(program, "access_constant");

		std::vector<cl_uint> accesses;
		std::vector<cl_uint> threads;
		for (cl_uint i = 1; i <= 32; i *= 2) {
			accesses.push_back(i);
		}
		for (cl_uint i = 32; i <= 512; i += 32) {
			threads.push_back(i);
		}
		std::vector<cl_uint> tests = {1, 2, 3};
		
		test_global_memory(accesses, threads, tests);
		test_local_memory(accesses, threads, tests);
		test_constant_memory(accesses, threads, tests);

		std::cout << "Microbenchmark #7 (broadcasting and parallel access) done." << std::endl;
	}	
	catch (cl::Error error)	{
		std::cout << error.what() << "(" <<
			error.err() << ")" << std::endl;
	}
	return 0;
} 