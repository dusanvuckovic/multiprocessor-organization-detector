#define CL_CLOCK(reg) asm volatile("mov.u64 %0, %%clock64;" : "=l" (reg))

__kernel void access_global(__global uint* in1, __global uint* in2, __global ulong* elapsed_time,
 uint step, uint iterations) {
	uint p, q;
	ulong start_time, stop_time;
	uint idx = get_global_id(0);
	q = idx / step * step;
	CL_CLOCK(start_time);	
	for (int i = 0; i < iterations; i++) {
		p = in1[q];
		q = in2[p];	
	}
	CL_CLOCK(stop_time);
	elapsed_time[idx] = stop_time - start_time;
}

__kernel void access_constant(__constant uint* in1, __constant uint* in2, __global ulong* elapsed_time,
 uint step, uint iterations) {
	uint p, q;
	ulong start_time, stop_time;
	uint idx = get_global_id(0);
	q = idx / step * step;
	CL_CLOCK(start_time);
	for (int i = 0; i < iterations; i++) {
		p = in1[q];
		q = in2[p];
	}
	CL_CLOCK(stop_time);	 
	elapsed_time[idx] = stop_time - start_time;
}

__kernel void access_local(__global uint* in1g, __global uint* in2g, __global ulong* elapsed_time,
 uint step, uint iterations, __local uint* in1, __local uint* in2) {
	uint num_of_threads = get_global_size(0);
	for (int i = 0; i < num_of_threads; i++) {
		in1[i] = in1g[i];
		in2[i] = in2g[i];
	}
	uint p, q;
	ulong start_time, stop_time;
	uint idx = get_global_id(0);
	q = idx / step * step;
	CL_CLOCK(start_time);
	for (int i = 0; i < iterations; i++) {
		p = in1[q];
		q = in2[p];
	}
	CL_CLOCK(stop_time);
	elapsed_time[idx] = stop_time - start_time;
}