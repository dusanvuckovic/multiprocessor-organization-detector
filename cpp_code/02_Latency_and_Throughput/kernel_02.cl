
#define REPEAT64(S) S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S
#define REPEAT128(S) REPEAT64(S) REPEAT64(S)
#define REPEAT256(S) REPEAT128(S) REPEAT128(S)
#define REPEAT512(S) REPEAT256(S) REPEAT256(S)
#define REPEAT1024(S) REPEAT512(S) REPEAT512(S)

#define ADD(A, B) A+=B
#define SUB(A, B) A-=B
#define MAX(A, B) max(A, B)
#define MIN(A, B) min(A, B)
#define MUL(A, B) A*=B
#define DIV(A, B) A/=B
#define MAD(A, B) A=A*B+A
#define REM(A, B) A%=B

#define AND(A, B) A&=B
#define OR(A, B) A|=B
#define XOR(A, B) A^=B
#define SHL(A, B) A<<=B
#define SHR(A, B) A>>=B

#define ABS(A) abs(A)
#define NEG(A) A=~A

#define SIN(A) A=sin(A)
#define COS(A) A=cos(A)
#define TAN(A) A=tan(A)
#define EXP(A) A=exp(A)
#define EXP2(A) A=exp2(A)
#define EXP10(A) A=exp10(A)
#define LOG(A) A=log(A)
#define LOG2(A) A=log2(A)
#define LOG10(A) A=log10(A)
#define SQRT(A) A=sqrt(A)

#define NSIN(A) A=native_sin(A)
#define NCOS(A) A=native_cos(A)
#define NTAN(A) A=native_tan(A)
#define NEXP(A) A=native_exp(A)
#define NEXP2(A) A= native_exp2(A)
#define NEXP10(A) A=native_exp10(A)
#define NLOG(A) A=native_log(A)
#define NLOG2(A) A=native_log2(A)
#define NLOG10(A) A=native_log10(A)
#define NSQRT(A) A=native_sqrt(A)
#define NDIV(A, B) A=native_divide(A, B)

#define MUL24(A, B) mul24(A, B)
#define MAD24(A, B) mad24(A, B, A)

#define CL_CLOCK(reg) asm volatile("mov.u64 %0, %%clock64;" : "=l" (reg))

/* Preprocessor-based macro generating specific kernel functions which are later ran to gauge performance. */
#define TEST2(OP, TYPE, ITER, REPT)\
	__kernel void measure_performance_##OP##_##TYPE##_##ITER##_##REPT##_K(__global ulong* elapsed_time) {\
		volatile TYPE t1 = 3, t2 = 5;\
		int idx = get_global_id(0);\
		ulong start_time = 0, stop_time = 1;\
		for (int i = 0; i < ITER; i++) {\
			CL_CLOCK(start_time);\
			REPEAT##REPT(OP(t1, t2); OP(t2, t1);)\
			CL_CLOCK(stop_time);\
		}\
		elapsed_time[idx] = stop_time - start_time;\
	}

#define TEST1(OP, TYPE, ITER, REPT)\
	__kernel void measure_performance_##OP##_##TYPE##_##ITER##_##REPT##_K(__global ulong* elapsed_time) {\
		volatile TYPE t1 = 3;\
		int idx = get_global_id(0);\
		ulong start_time = 0, stop_time = 1;\
		for (int i = 0; i < ITER; i++) {\
			CL_CLOCK(start_time);\
			REPEAT##REPT(OP(t1); OP(t1);)\
			CL_CLOCK(stop_time);\
		}\
		elapsed_time[idx] = stop_time - start_time;\
	}


// Actual generating of kernel functions.
TEST2(ADD, uint, 1024, 64)
TEST2(SUB, uint, 1024, 64)
TEST2(MAX, uint, 1024, 64)
TEST2(MIN, uint, 1024, 64)
TEST2(MUL, uint, 1024, 64)
TEST2(DIV, uint, 1024, 64)
TEST2(MAD, uint, 1024, 64)
TEST2(REM, uint, 1024, 64)

TEST2(ADD, int, 1024, 64)
TEST2(SUB, int, 1024, 64)
TEST2(MAX, int, 1024, 64)
TEST2(MIN, int, 1024, 64)
TEST2(MUL, int, 1024, 64)
TEST2(DIV, int, 1024, 64)
TEST2(MAD, int, 1024, 64)
TEST2(REM, int, 1024, 64)

TEST2(ADD, long, 1024, 64)
TEST2(SUB, long, 1024, 64)
TEST2(MAX, long, 1024, 64)
TEST2(MIN, long, 1024, 64)
TEST2(MUL, long, 1024, 64)
TEST2(DIV, long, 1024, 64)
TEST2(MAD, long, 1024, 64)
TEST2(REM, long, 1024, 64)

TEST2(ADD, ulong, 1024, 64)
TEST2(SUB, ulong, 1024, 64)
TEST2(MAX, ulong, 1024, 64)
TEST2(MIN, ulong, 1024, 64)
TEST2(MUL, ulong, 1024, 64)
TEST2(DIV, ulong, 1024, 64)
TEST2(MAD, ulong, 1024, 64)
TEST2(REM, ulong, 1024, 64)

TEST2(ADD, float, 1024, 64)
TEST2(SUB, float, 1024, 64)
TEST2(MAX, float, 1024, 64)
TEST2(MIN, float, 1024, 64)
TEST2(MUL, float, 1024, 64)
TEST2(DIV, float, 1024, 64)
TEST2(MAD, float, 1024, 64)

TEST2(ADD, double, 1024, 64)
TEST2(SUB, double, 1024, 64)
TEST2(MAX, double, 1024, 64)
TEST2(MIN, double, 1024, 64)
TEST2(MUL, double, 1024, 64)
TEST2(DIV, double, 1024, 64)
TEST2(MAD, double, 1024, 64)

TEST2(AND, uint, 1024, 64)
TEST2(OR, uint, 1024, 64)
TEST2(XOR, uint, 1024, 64)
TEST2(SHL, uint, 1024, 64)
TEST2(SHR, uint, 1024, 64)

TEST2(AND, ulong, 1024, 64)
TEST2(OR, ulong, 1024, 64)
TEST2(XOR, ulong, 1024, 64)
TEST2(SHL, ulong, 1024, 64)
TEST2(SHR, ulong, 1024, 64)

TEST1(ABS, uint, 1024, 64);
TEST1(NEG, uint, 1024, 64);

TEST1(ABS, ulong, 1024, 64);
TEST1(NEG, ulong, 1024, 64);

TEST1(SIN, float, 1024, 64);
TEST1(COS, float, 1024, 64);
TEST1(TAN, float, 1024, 64);
TEST1(EXP, float, 1024, 64);
TEST1(EXP2, float, 1024, 64);
TEST1(EXP10, float, 1024, 64);
TEST1(LOG, float, 1024, 64);
TEST1(LOG2, float, 1024, 64);
TEST1(LOG10, float, 1024, 64);
TEST1(SQRT, float, 1024, 64);

TEST1(SIN, double, 1024, 64);
TEST1(COS, double, 1024, 64);
TEST1(TAN, double, 1024, 64);
TEST1(EXP, double, 1024, 64);
TEST1(EXP2, double, 1024, 64);
TEST1(EXP10, double, 1024, 64);
TEST1(LOG, double, 1024, 64);
TEST1(LOG2, double, 1024, 64);
TEST1(LOG10, double, 1024, 64);
TEST1(SQRT, double, 1024, 64);

TEST1(NSIN, float, 1024, 64);
TEST1(NCOS, float, 1024, 64);
TEST1(NTAN, float, 1024, 64);
TEST1(NEXP, float, 1024, 64);
TEST1(NEXP2, float, 1024, 64);
TEST1(NEXP10, float, 1024, 64);
TEST1(NLOG, float, 1024, 64);
TEST1(NLOG2, float, 1024, 64);
TEST1(NLOG10, float, 1024, 64);
TEST1(NSQRT, float, 1024, 64);
TEST2(NDIV, float, 1024, 64);

TEST1(NSIN, double, 1024, 64);
TEST1(NCOS, double, 1024, 64);
TEST1(NTAN, double, 1024, 64);
TEST1(NEXP, double, 1024, 64);
TEST1(NEXP2, double, 1024, 64);
TEST1(NEXP10, double, 1024, 64);
TEST1(NLOG, double, 1024, 64);
TEST1(NLOG2, double, 1024, 64);
TEST1(NLOG10, double, 1024, 64);
TEST1(NSQRT, double, 1024, 64);
TEST2(NDIV, double, 1024, 64);

TEST2(MUL24, uint, 1024, 64);
TEST2(MAD24, uint, 1024, 64);