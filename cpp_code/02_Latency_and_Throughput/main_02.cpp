#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

std::string output_filename = "../02_latency_and_throughput.txt";

int platform_number = 0;
int device_number = 0;
unsigned int device_type = CL_DEVICE_TYPE_GPU;
const bool has_elapsed_time = true;

/* This microbenchmark calculates the latency and throughput of operations
 * always supported by the OpenCL runtime. Through the overview of results * the
 * programmer is made aware of his device's advantages and disadvantages when it
 * comes to processing various data types and various combinations. The results
 * are written into the "latency_and_throughput.txt" file as tuples. Graphs to
 * be presented later are native v. regular functions, highest throughput
 * functions, overview by operation and by type, etc.
 */

// Elements in global scope for ease of access in functions.
const int N_ELEMENTS = 1024;
cl_ulong* elapsed_time;
cl::CommandQueue queue;
cl::Program program;
cl::Buffer buffer;
std::ofstream file(output_filename);
const cl_ulong MILLION = 1000000;
cl_int max_workgroup;
std::vector<cl::Platform> platforms;
std::vector<cl::Device> devices;

struct Measurement {
  std::string operation;
  std::string type;
  cl_ulong iterations;
  cl_ulong repetitions;
  cl_double latency;
  cl_ulong latency_in_clocks;
  cl_double throughput;
  cl_double throughput_in_clocks;

Measurement(std::string full_description) {
    std::string s = full_description;
    s = s.substr(s.find('_') + 1);
    s = s.substr(s.find('_') + 1);
    this->operation = s.substr(0, s.find('_'));
    s = s.substr(s.find('_') + 1);
    this->type = s.substr(0, s.find('_'));
    s = s.substr(s.find('_') + 1);
    this->iterations = std::stoi(s.substr(0, s.find('_')));
    s = s.substr(s.find('_') + 1);
    this->repetitions = std::stoi(s.substr(0, s.find('_')));
    this->latency = 0;
    this->throughput = 0;
	this->latency_in_clocks = 0;
	this->throughput_in_clocks = 0;
  }

  std::string write(bool has_clock) {
    std::ostringstream output;
    if (has_clock) {
      output << "(" << operation << ", " << type << ", " << iterations << ", "
             << repetitions << ", " << latency << ", "
             << latency_in_clocks << ", " << throughput << ", "
             << throughput_in_clocks << ")";
    } else {
      output << "(" << operation << ", " << type << ", " << iterations << ", "
             << repetitions << ", " << latency << ", " << throughput << ")";
    }
    return output.str();
  }
};

void measure_latency(const cl::Kernel& my_kernel, Measurement& measurement) {
  cl::NDRange global(1);
  cl::NDRange local(1);
  cl::Event time_event;
  queue.enqueueNDRangeKernel(my_kernel, cl::NullRange, global, local,
                             nullptr, &time_event);
  time_event.wait();
  cl_double elapsed = time_event.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                      time_event.getProfilingInfo<CL_PROFILING_COMMAND_START>();
  measurement.latency = elapsed / MILLION;  // in ms
  queue.enqueueReadBuffer(buffer, CL_TRUE, 0,
                          N_ELEMENTS * sizeof(cl_ulong), elapsed_time);
  measurement.latency_in_clocks = (double)((elapsed_time[0] / (measurement.repetitions * 2));
}

void measure_throughput(const cl::Kernel& my_kernel, Measurement& measurement, size_t multiples) {
  cl::NDRange global(N_ELEMENTS);
  cl::NDRange local(N_ELEMENTS / multiples);
  cl::Event time_event;
  queue.enqueueNDRangeKernel(my_kernel, cl::NullRange, global, local,
                             nullptr, &time_event);
  time_event.wait();
  cl_double elapsed = time_event.getProfilingInfo<CL_PROFILING_COMMAND_END>() -
                      time_event.getProfilingInfo<CL_PROFILING_COMMAND_START>();
  elapsed /= MILLION;
  queue.enqueueReadBuffer(buffer, CL_TRUE, 0,
                          N_ELEMENTS * sizeof(cl_ulong), elapsed_time);
  measurement.throughput = (measurement.repetitions * 2 * measurement.iterations) / elapsed;  // operations/ms
  measurement.thoughput_in_clocks = (double)elapsed_time[0] / (measurement.repetitions * 2);  // elapsed_time
}

void measure_performance(const std::string& operation {
  cl::Device& device = devices[device_number];
  cl::Kernel my_kernel(program, operation.c_str());
  my_kernel.setArg(0, buffer);
  auto value = my_kernel.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(device);

  Measurement m{operation};
  measure_latency(my_kernel, m);
  measure_throughput(my_kernel, m, value);
  file << m.write(has_elapsed_time) << std::endl;
}

int main() {
  elapsed_time = new cl_ulong[N_ELEMENTS];
  for (auto i = 0; i < N_ELEMENTS; i++) {
    elapsed_time[i] = 0;
  }
  try {
    cl::Platform::get(&platforms);
    platforms[platform_number].getDevices(device_type, &devices);

    cl::Context context(devices);
    queue = cl::CommandQueue(context,
                             devices[device_number] CL_QUEUE_PROFILING_ENABLE);
    buffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, N_ELEMENTS * sizeof(cl_ulong));

    std::ifstream sourceFile("kernel_02.cl");
    std::string sourceCode(std::istreambuf_iterator<char>(sourceFile)(
        std::istreambuf_iterator<char>()));
    cl::Program::Sources source(
        1, std::make_pair(sourceCode.c_str(), sourceCode.length() + 1));
    program = cl::Program(context, source);
    program.build(devices, "-cl-opt-disable");

    measure_performance("measure_performance_ADD_uint_1024_64_K");
    measure_performance("measure_performance_SUB_uint_1024_64_K");
    measure_performance("measure_performance_MAX_uint_1024_64_K");
    measure_performance("measure_performance_MIN_uint_1024_64_K");
    measure_performance("measure_performance_MUL_uint_1024_64_K");
    measure_performance("measure_performance_DIV_uint_1024_64_K");
    measure_performance("measure_performance_MAD_uint_1024_64_K");
    measure_performance("measure_performance_REM_uint_1024_64_K");

    measure_performance("measure_performance_ADD_int_1024_64_K");
    measure_performance("measure_performance_SUB_int_1024_64_K");
    measure_performance("measure_performance_MAX_int_1024_64_K");
    measure_performance("measure_performance_MIN_int_1024_64_K");
    measure_performance("measure_performance_MUL_int_1024_64_K");
    measure_performance("measure_performance_DIV_int_1024_64_K");
    measure_performance("measure_performance_MAD_int_1024_64_K");
    measure_performance("measure_performance_REM_int_1024_64_K");

    measure_performance("measure_performance_ADD_long_1024_64_K");
    measure_performance("measure_performance_SUB_long_1024_64_K");
    measure_performance("measure_performance_MAX_long_1024_64_K");
    measure_performance("measure_performance_MIN_long_1024_64_K");
    measure_performance("measure_performance_MUL_long_1024_64_K");
    measure_performance("measure_performance_DIV_long_1024_64_K");
    measure_performance("measure_performance_MAD_long_1024_64_K");
    measure_performance("measure_performance_REM_long_1024_64_K");

    measure_performance("measure_performance_ADD_ulong_1024_64_K");
    measure_performance("measure_performance_SUB_ulong_1024_64_K");
    measure_performance("measure_performance_MAX_ulong_1024_64_K");
    measure_performance("measure_performance_MIN_ulong_1024_64_K");
    measure_performance("measure_performance_MUL_ulong_1024_64_K");
    measure_performance("measure_performance_DIV_ulong_1024_64_K");
    measure_performance("measure_performance_MAD_ulong_1024_64_K");
    measure_performance("measure_performance_REM_ulong_1024_64_K");

    measure_performance("measure_performance_ADD_float_1024_64_K");
    measure_performance("measure_performance_SUB_float_1024_64_K");
    measure_performance("measure_performance_MAX_float_1024_64_K");
    measure_performance("measure_performance_MIN_float_1024_64_K");
    measure_performance("measure_performance_MUL_float_1024_64_K");
    measure_performance("measure_performance_DIV_float_1024_64_K");
    measure_performance("measure_performance_MAD_float_1024_64_K");

    measure_performance("measure_performance_ADD_double_1024_64_K");
    measure_performance("measure_performance_SUB_double_1024_64_K");
    measure_performance("measure_performance_MAX_double_1024_64_K");
    measure_performance("measure_performance_MIN_double_1024_64_K");
    measure_performance("measure_performance_MUL_double_1024_64_K");
    measure_performance("measure_performance_DIV_double_1024_64_K");
    measure_performance("measure_performance_MAD_double_1024_64_K");

    measure_performance("measure_performance_AND_uint_1024_64_K");
    measure_performance("measure_performance_OR_uint_1024_64_K");
    measure_performance("measure_performance_XOR_uint_1024_64_K");
    measure_performance("measure_performance_SHL_uint_1024_64_K");
    measure_performance("measure_performance_SHR_uint_1024_64_K");

    measure_performance("measure_performance_AND_ulong_1024_64_K");
    measure_performance("measure_performance_OR_ulong_1024_64_K");
    measure_performance("measure_performance_XOR_ulong_1024_64_K");
    measure_performance("measure_performance_SHL_ulong_1024_64_K");
    measure_performance("measure_performance_SHR_ulong_1024_64_K");

    measure_performance("measure_performance_ABS_uint_1024_64_K");
    measure_performance("measure_performance_NEG_uint_1024_64_K");

    measure_performance("measure_performance_ABS_ulong_1024_64_K");
    measure_performance("measure_performance_NEG_ulong_1024_64_K");

    measure_performance("measure_performance_SIN_float_1024_64_K");
    measure_performance("measure_performance_COS_float_1024_64_K");
    measure_performance("measure_performance_TAN_float_1024_64_K");
    measure_performance("measure_performance_EXP_float_1024_64_K");
    measure_performance("measure_performance_EXP2_float_1024_64_K");
    measure_performance("measure_performance_EXP10_float_1024_64_K");
    measure_performance("measure_performance_LOG_float_1024_64_K");
    measure_performance("measure_performance_LOG2_float_1024_64_K");
    measure_performance("measure_performance_LOG10_float_1024_64_K");
    measure_performance("measure_performance_SQRT_float_1024_64_K");

    measure_performance("measure_performance_SIN_double_1024_64_K");
    measure_performance("measure_performance_COS_double_1024_64_K");
    measure_performance("measure_performance_TAN_double_1024_64_K");
    measure_performance("measure_performance_EXP_double_1024_64_K");
    measure_performance("measure_performance_EXP2_double_1024_64_K");
    measure_performance("measure_performance_EXP10_double_1024_64_K");
    measure_performance("measure_performance_LOG_double_1024_64_K");
    measure_performance("measure_performance_LOG2_double_1024_64_K");
    measure_performance("measure_performance_LOG10_double_1024_64_K");
    measure_performance("measure_performance_SQRT_double_1024_64_K");

    measure_performance("measure_performance_NSIN_float_1024_64_K");
    measure_performance("measure_performance_NCOS_float_1024_64_K");
    measure_performance("measure_performance_NTAN_float_1024_64_K");
    measure_performance("measure_performance_NEXP_float_1024_64_K");
    measure_performance("measure_performance_NEXP2_float_1024_64_K");
    measure_performance("measure_performance_NEXP10_float_1024_64_K");
    measure_performance("measure_performance_NLOG_float_1024_64_K");
    measure_performance("measure_performance_NLOG2_float_1024_64_K");
    measure_performance("measure_performance_NLOG10_float_1024_64_K");
    measure_performance("measure_performance_NSQRT_float_1024_64_K");
    measure_performance("measure_performance_NDIV_float_1024_64_K");

    measure_performance("measure_performance_NSIN_double_1024_64_K");
    measure_performance("measure_performance_NCOS_double_1024_64_K");
    measure_performance("measure_performance_NTAN_double_1024_64_K");
    measure_performance("measure_performance_NEXP_double_1024_64_K");
    measure_performance("measure_performance_NEXP2_double_1024_64_K");
    measure_performance("measure_performance_NEXP10_double_1024_64_K");
    measure_performance("measure_performance_NLOG_double_1024_64_K");
    measure_performance("measure_performance_NLOG2_double_1024_64_K");
    measure_performance("measure_performance_NLOG10_double_1024_64_K");
    measure_performance("measure_performance_NSQRT_double_1024_64_K");
    measure_performance("measure_performance_NDIV_double_1024_64_K");

    measure_performance("measure_performance_MAD24_uint_1024_64_K");
    measure_performance("measure_performance_MUL24_uint_1024_64_K");

    file.close();

    delete[] elapsed_time;
    
    std::cout << "Microbenchmark #2 (latency and throughput) done." << std::endl;
  } 
  catch (cl::Error error) {
    std::cout << error.what() << "(" << error.err() << ")" << std::endl;
  }
  return 0;
}