#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <chrono>

/* This microbenchmark tests the allocation speeds of pinned and non-pinned memory.
 * The output is the allocation times for combinations of (read, write, readwrite)
 * and (pinned, unpinned) for various allocation sizes.
 */

int platform_number = 0;
int device_number = 0;
unsigned int device_type = CL_DEVICE_TYPE_GPU;

const std::string output_filename = "../06_pinned_v_unpinned.txt";

/* A class used to measure execution times on the CPU. */
class Measure {
	std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	std::chrono::high_resolution_clock::time_point end;

public:
	void reset() {
		std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
	}

	void finish() {
		end = std::chrono::high_resolution_clock::now();
	}

	// Return elapsed time in ms.
	double finish_and_measure() { 
		std::chrono::duration<double, std::milli> fp_ms = std::chrono::high_resolution_clock::now() - start;
		return fp_ms.count();
	}

	double measure() {
		std::chrono::duration<double, std::milli> fp_ms = end - start;
		return fp_ms.count();
	}
};

double allocate_pinned(cl::Context& context, cl::CommandQueue& queue, const cl_ulong sz, const cl_ulong flags) {
	cl::Buffer deviceBuffer = cl::Buffer(context, flags | CL_MEM_ALLOC_HOST_PTR, sz * sizeof(cl_uint), nullptr);
	cl::Buffer hostBuffer = cl::Buffer(context, flags | CL_MEM_ALLOC_HOST_PTR, sz * sizeof(cl_uint), nullptr);
	cl_uint* hostArray = (cl_uint*)queue.enqueueMapBuffer(hostBuffer, CL_TRUE, CL_MAP_WRITE, 0, sz * sizeof(cl_uint));
	for (cl_uint i = 0; i < sz; i++) {
		hostArray[i] = i;
	}
	Measure elapsed;
	queue.enqueueWriteBuffer(deviceBuffer, CL_TRUE, 0, sz * sizeof(cl_uint), hostArray);
	elapsed.finish();
	queue.enqueueUnmapMemObject(hostBuffer, hostArray);
	return elapsed.measure();
}

double allocate_unpinned(cl::Context& context, cl::CommandQueue& queue, const cl_ulong sz, const cl_ulong flags) {
	cl_uint* data = new cl_uint[sz];
	for (int i = 0; i < sz; i++)
		data[i] = i;
	cl::Buffer buffer = cl::Buffer(context, flags, sz * sizeof(cl_uint));
	Measure elapsed;
	queue.enqueueWriteBuffer(buffer, CL_TRUE, 0, sz * sizeof(cl_uint), data);
	elapsed.finish();

	delete[] data;
	return elapsed.measure();
}

int main() {
	try {
		std::vector<cl::Platform>platforms;
		cl::Platform::get(&platforms);

		std::vector<cl::Device>devices;
		platforms[platform_number].getDevices(device_type, &devices);

		cl::Context context(devices);
		auto max_global_memory = devices[device_number].getInfo<CL_DEVICE_MAX_MEM_ALLOC_SIZE>();
		cl::CommandQueue queue =
			cl::CommandQueue(context, devices[device_number]);
	
		std::ofstream file(output_filename);		
		for (auto i = 1024; i < max_global_memory/sizeof(cl_uint); i *= 2) {
			file << "(" << i << ", ";
			file << allocate_pinned(context, queue, i, CL_MEM_READ_ONLY) << ", ";
			file << allocate_pinned(context, queue, i, CL_MEM_WRITE_ONLY) << ", ";
			file << allocate_pinned(context, queue, i,  CL_MEM_READ_WRITE) << ", ";
			file << allocate_unpinned(context, queue, i, CL_MEM_READ_ONLY) << ", ";
			file << allocate_unpinned(context, queue, i, CL_MEM_WRITE_ONLY) << ", ";
			file << allocate_unpinned(context, queue, i, CL_MEM_READ_WRITE) << ")" << std::endl;
		}

		std::cout << "Microbenchmark #6 (pinned v. unpinned) done." << std::endl;
	}

	catch (cl::Error error) {
		std::cout << error.what() << "(" <<
			error.err() << ")" << std::endl;
	}
	return 0;
}