#define TOTAL_ITERATIONS (100000)

__kernel void simt_deadlock(__global ulong* result) {
	__local uint counter;
	__local ulong iterations;
	ulong TOTAL_THREADS = get_global_size(0);
	counter = 0;
	iterations = 0;
	int idx = get_global_id(0);	
	
	barrier(CLK_LOCAL_MEM_FENCE);

	while (counter != TOTAL_THREADS && iterations <= TOTAL_ITERATIONS) {
		if (idx == counter) {
			counter++;
		}
		else {
			iterations++;
		}
	}
	ulong r = (iterations < TOTAL_ITERATIONS) ? 1 : 0;
	result[idx] = r;
}