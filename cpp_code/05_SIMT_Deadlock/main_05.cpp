#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <algoritm>

/* This microbenchmark attempts to cause a "deadlock" by serializing threads 
 * in a coarse manner inside the kernel. Since the deadlock would mean the program 
 * would crash, we run it through a large number (100000) of iterations instead, and 
 * consider that it would have deadlocked if the kernel doesn't complete by then.
 * The output is the state (no crash, crash).
 */

std::string output_filename = "../05_simt_deadlock.txt";

int platform_number = 0;
int device_number = 0;
unsigned int device_type = CL_DEVICE_TYPE_GPU;

int main() {	
	try {
		std::vector<cl::Platform>platforms;
		cl::Platform::get(&platforms);

		std::vector<cl::Device>devices;
		platforms[platform_number].getDevices(device_type, &devices);

		cl::Context context(devices);
		cl::CommandQueue queue =
			cl::CommandQueue(context, devices[device_number]);

		std::ifstream sourceFile("kernel_05.cl");
		std::string sourceCode(
			std::istreambuf_iterator<char>(sourceFile),
			(std::istreambuf_iterator<char>()));
		cl::Program::Sources source(1,
			std::make_pair(sourceCode.c_str(),
				sourceCode.length() + 1));
		cl::Program program = cl::Program(context, source);
		program.build(devices, "-cl-opt-disable");

		cl::Kernel my_kernel(program, "simt_deadlock");
		cl_uint N_ELEMENTS = 
			std::min(32, my_kernel.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(devices[0]));
		cl_ulong* res = new cl_ulong[N_ELEMENTS];
		for (auto i = 0; i < N_ELEMENTS; i++) {
			res[i] = ~0;
		}
		cl::Buffer bufferResult = cl::Buffer(context,
			CL_MEM_READ_WRITE, sizeof(cl_ulong) * N_ELEMENTS);
		queue.enqueueWriteBuffer(bufferResult, CL_TRUE, 0,
			sizeof(cl_ulong) * N_ELEMENTS, res);

		my_kernel.setArg(0, bufferResult);
		cl::NDRange global(N_ELEMENTS);
		cl::NDRange local(N_ELEMENTS);
		queue.enqueueNDRangeKernel(my_kernel,
			cl::NullRange, cl::NDRange(N_ELEMENTS), cl::NDRange(N_ELEMENTS));

		queue.enqueueReadBuffer(bufferResult, CL_TRUE, 0,
			N_ELEMENTS * sizeof(cl_ulong), res);
		std::ofstream file(output_filename);
		for (int i = 0; i < N_ELEMENTS; i++) {
			file << res[i] << " ";
		}
		file.close();
		delete[] res;
		std::cout << "Microbenchmark #5 (SIMT deadlock) done." << std::endl;
	}
	catch (cl::Error error) {
		std::cout << error.what() << "(" <<
			error.err() << ")" << std::endl;
	}
	return 0;
}