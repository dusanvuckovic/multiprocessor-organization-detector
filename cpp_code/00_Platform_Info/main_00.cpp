#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <map>
#include <memory>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdint>

/* This microbenchmark accesses the pertinent properties of the device reachable
*  through OpenCL. Those properties are written into a text file named 
*  "device_properties.txt".
*/

std::string output_filename = "../device_properties_INTEL.txt";

cl_uint platform_labels[] = {
	CL_PLATFORM_NAME,
	CL_PLATFORM_VENDOR,
	CL_PLATFORM_VERSION,
	CL_PLATFORM_PROFILE,
	CL_PLATFORM_EXTENSIONS};

cl_uint device_labels_string[] = {
	CL_DEVICE_EXTENSIONS,
	CL_DEVICE_NAME,
	CL_DEVICE_OPENCL_C_VERSION,
	CL_DEVICE_PROFILE,
	CL_DEVICE_VENDOR,
	CL_DEVICE_VERSION,
	CL_DRIVER_VERSION};

cl_uint device_labels_uint[] = {
	CL_DEVICE_ADDRESS_BITS,
	CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
	CL_DEVICE_MAX_CLOCK_FREQUENCY,
	CL_DEVICE_MAX_COMPUTE_UNITS,
	CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
	CL_DEVICE_MEM_BASE_ADDR_ALIGN,
	CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
	CL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
	CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
	CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
	CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
	CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
	CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE,
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,
};

cl_uint device_labels_bool[] = {
	CL_DEVICE_ENDIAN_LITTLE,
	CL_DEVICE_HOST_UNIFIED_MEMORY,
};

cl_uint device_labels_ulong[] = {
	CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
	CL_DEVICE_GLOBAL_MEM_SIZE,
	CL_DEVICE_LOCAL_MEM_SIZE,
	CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
	CL_DEVICE_MAX_MEM_ALLOC_SIZE,
};

cl_uint device_labels_sizet[] = {
	CL_DEVICE_MAX_PARAMETER_SIZE,
	CL_DEVICE_MAX_WORK_GROUP_SIZE,
	CL_DEVICE_PROFILING_TIMER_RESOLUTION,
};

cl_uint device_labels_enum[] = {
	CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
	CL_DEVICE_LOCAL_MEM_TYPE,
};

cl_uint device_labels_bitfield[] = {
	CL_DEVICE_TYPE,
	CL_DEVICE_SINGLE_FP_CONFIG,
	CL_DEVICE_DOUBLE_FP_CONFIG,
};

cl_uint device_labels_vectors[] = {
	CL_DEVICE_MAX_WORK_ITEM_SIZES};

cl_uint device_labels_ordered[] = {
	CL_PLATFORM_NAME,	//"Platform name"
	CL_PLATFORM_VENDOR,  //"Platform vendor"
	CL_PLATFORM_VERSION, //"Platform version"
	CL_PLATFORM_PROFILE, //"Platform profile"

	CL_DEVICE_VENDOR,			//"Device vendor"
	CL_DEVICE_NAME,				//"Device name"
	CL_DEVICE_TYPE,				//"Device types"
	CL_DEVICE_VERSION,			//"Device version"
	CL_DEVICE_OPENCL_C_VERSION, //"Device OpenCL version"
	CL_DRIVER_VERSION,			//"Driver version"
	CL_DEVICE_PROFILE,			//"Device profile"

	CL_DEVICE_ADDRESS_BITS,				 //"Address space size"
	CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, //"Global memory cache line (B)"
	CL_DEVICE_MAX_CLOCK_FREQUENCY,		 //"Max clock frequency (MHz)"
	CL_DEVICE_MAX_COMPUTE_UNITS,		 //"Number of parallel compute units"
	CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,  //"Maximum number of dimensions for work-items"
	CL_DEVICE_MAX_WORK_GROUP_SIZE,		 //"Maximum number of work-items in a work-group"
	CL_DEVICE_MAX_WORK_ITEM_SIZES,		 //"Maximum number of work-items per dimension"

	CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,		 //"Native char width"
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,   //"Preferred char width"
	CL_DEVICE_NATIVE_VECTOR_WIDTH_INT,		 //"Native int width"
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,	//"Preferred int width"
	CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,		 //"Native long width"
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,   //"Preferred long width"
	CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,	 //"Native float width"
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,  //"Preferred float width"
	CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,	//"Native double width"
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, //"Preferred double width"
	CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,	 //"Native short width"
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,  //"Preferred short width"
	CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,		 //"Native half width"
	CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,   //"Preferred half width"

	CL_DEVICE_GLOBAL_MEM_SIZE,			//"Global memory size (B)"
	CL_DEVICE_MAX_MEM_ALLOC_SIZE,		//"Maximum global memory object allocation"
	CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,	//"Global memory cache size (B)"
	CL_DEVICE_LOCAL_MEM_SIZE,			//"Local memory cache size (B)"
	CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, //"Maximum constant buffer allocation"
	CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,	//"Type of global memory cache"
	CL_DEVICE_LOCAL_MEM_TYPE,			//"Type of local memory"

	CL_DEVICE_MAX_PARAMETER_SIZE,		  //"Maximum kernel parameter size"
	CL_DEVICE_PROFILING_TIMER_RESOLUTION, //"Resolution of the profiling timer (ns)"

	CL_DEVICE_SINGLE_FP_CONFIG, //"Single-precision floating-point options"
	CL_DEVICE_DOUBLE_FP_CONFIG, //"Double-precision floating-point options"

	CL_PLATFORM_EXTENSIONS, //"Platform extensions"
	CL_DEVICE_EXTENSIONS,   //"Device extensions"
};

std::map<cl_ulong, std::string> descriptions = {
	{CL_PLATFORM_NAME, "Platform name"},
	{CL_PLATFORM_VENDOR, "Platform vendor"},
	{CL_PLATFORM_VERSION, "Platform version"},
	{CL_PLATFORM_PROFILE, "Platform profile"},
	{CL_PLATFORM_EXTENSIONS, "Platform extensions"},

	{CL_DEVICE_VENDOR, "Device vendor"},
	{CL_DEVICE_NAME, "Device name"},
	{CL_DEVICE_VERSION, "Device version"},
	{CL_DEVICE_OPENCL_C_VERSION, "Device OpenCL version"},
	{CL_DRIVER_VERSION, "Driver version"},
	{CL_DEVICE_PROFILE, "Device profile"},
	{CL_DEVICE_EXTENSIONS, "Device extensions"},

	{CL_DEVICE_ADDRESS_BITS, "Address space size"},
	{CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE, "Global memory cache line (B)"},
	{CL_DEVICE_MAX_CLOCK_FREQUENCY, "Max clock frequency (MHz)"},
	{CL_DEVICE_MAX_COMPUTE_UNITS, "Number of parallel compute units"},
	{CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, "Maximum number of dimensions for work-items"},

	{CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR, "Native char width"},
	{CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR, "Preferred char width"},
	{CL_DEVICE_NATIVE_VECTOR_WIDTH_INT, "Native int width"},
	{CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT, "Preferred int width"},
	{CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG, "Native long width"},
	{CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG, "Preferred long width"},
	{CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT, "Native float width"},
	{CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT, "Preferred float width"},
	{CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE, "Native double width"},
	{CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE, "Preferred double width"},
	{CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT, "Native short width"},
	{CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT, "Preferred short width"},
	{CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF, "Native half width"},
	{CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF, "Preferred half width"},

	{CL_DEVICE_GLOBAL_MEM_SIZE, "Global memory size (B)"},
	{CL_DEVICE_MAX_MEM_ALLOC_SIZE, "Maximum global memory object allocation"},
	{CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, "Global memory cache size (B)"},
	{CL_DEVICE_LOCAL_MEM_SIZE, "Local memory cache size (B)"},
	{CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, "Maximum constant buffer allocation"},

	{CL_DEVICE_MAX_PARAMETER_SIZE, "Maximum kernel parameter size"},
	{CL_DEVICE_MAX_WORK_GROUP_SIZE, "Maximum number of work-items in a work-group"},
	{CL_DEVICE_PROFILING_TIMER_RESOLUTION, "Resolution of the profiling timer (ns)"},

	{CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, "Type of global memory cache"},
	{CL_DEVICE_LOCAL_MEM_TYPE, "Type of local memory"},

	{CL_DEVICE_SINGLE_FP_CONFIG, "Single-precision floating-point options"},
	{CL_DEVICE_DOUBLE_FP_CONFIG, "Double-precision floating-point options"},
	{CL_DEVICE_TYPE, "Device types"},

	{CL_DEVICE_MAX_WORK_ITEM_SIZES, "Maximum number of work-items per dimension"},
};

/**
 * A class holding the data read from the OpenCL runtime, and enabling
 * easy output to a stream. 
*/
class OCL_Data
{
public:
	OCL_Data(cl_uint macro, std::string description, std::string value)	{
		this->macro = macro;
		this->description = description;
		this->value = value;
	}

	std::string pretty_print() const {
		return description + ": " + value;
	}

	std::string get_value() const {
		return value;
	}

private:
	cl_uint macro;
	std::string description;
	std::string value;
};

int platform_number = 1;
int device_number = 0;
unsigned int device_type = CL_DEVICE_TYPE_CPU;

// The map of results: MACRO -> (MACRO, description, value)
std::map<cl_uint, std::unique_ptr<OCL_Data>> info;

int main() {
	try {
		// Query for platforms
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);
		std::cout << platforms.size() << std::endl;

		// Add platform values to map of results
		for (auto label : platform_labels) {
			std::string pf;
			platforms[platform_number].getInfo(label, &pf);
			info[label] = std::make_unique<OCL_Data>(label, descriptions[label], pf);
		}

		// Query for devices
		std::vector<cl::Device> devices;
		platforms[platform_number].getDevices(device_type, &devices);

		// Add device string values
		for (auto label : device_labels_string) {
			std::string pf;
			devices[device_number].getInfo(label, &pf);
			info[label] = std::make_unique<OCL_Data>(label, descriptions[label], pf);
		}

		// Add device uint values
		for (auto label : device_labels_uint) {
			cl_uint data;
			devices[device_number].getInfo(label, &data);
			info[label] = std::make_unique<OCL_Data>(label, descriptions[label], std::to_string(data));
		}

		// Add device ulong values
		for (auto label : device_labels_ulong) {
			cl_ulong data;
			devices[device_number].getInfo(label, &data);
			info[label] = std::make_unique<OCL_Data>(label, descriptions[label], std::to_string(data));
		}

		// Add device size_t values
		for (auto label : device_labels_sizet) {
			size_t sz;
			devices[device_number].getInfo(label, &sz);
			info[label] = std::make_unique<OCL_Data>(label, descriptions[label], std::to_string(sz));
		}

		// Add device enum values. Requires custom parsing.
		for (auto label : device_labels_enum) {
			cl_uint en;
			devices[device_number].getInfo(label, &en);
			std::string text;
			switch (label) {
			case CL_DEVICE_GLOBAL_MEM_CACHE_TYPE:
				if (en == CL_NONE)
					text = "No cache";
				else if (en == CL_READ_ONLY_CACHE)
					text = "Read-only cache";
				else if (en == CL_READ_WRITE_CACHE)
					text = "Read-write cache";
				else
					text = "ERROR";
				break;
			case CL_DEVICE_LOCAL_MEM_TYPE:
				if (en == CL_LOCAL)
					text = "Dedicated local memory";
				else if (en == CL_GLOBAL)
					text = "No dedicated local memory";
				else
					text = "ERROR";
				break;
			};
			info[label] = std::make_unique<OCL_Data>(label, descriptions[label], text);
		}

		// Add device bitfield values. Requires custom parsing.
		for (auto label : device_labels_bitfield) {
			cl_ulong bit_vector;
			devices[device_number].getInfo(label, &bit_vector);
			std::string text = "";
			switch (label) {
			case CL_DEVICE_TYPE:
				if (bit_vector & CL_DEVICE_TYPE_CPU)
					text += "CPU ";
				if (bit_vector & CL_DEVICE_TYPE_GPU)
					text += "GPU ";
				if (bit_vector & CL_DEVICE_TYPE_ACCELERATOR)
					text += "ACCELERATOR ";
				if (bit_vector & CL_DEVICE_TYPE_CUSTOM)
					text += "CUSTOM ";
				break;
			case CL_DEVICE_SINGLE_FP_CONFIG:
			case CL_DEVICE_DOUBLE_FP_CONFIG:
				if (bit_vector & CL_FP_FMA)
					text += "fused multiply-add";
				else
					text += "no fused multiply-add";
				break;
			}
			info[label] = std::make_unique<OCL_Data>(label, descriptions[label], text);
		}

		// Add the total number of dimensions
		std::vector<size_t> values;
		devices[device_number].getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, &values);
		std::string dimensions = "(";
		for (int i = 0; i < values.size(); i++)	{
			dimensions += std::to_string(values[i]);
			if (i != values.size() - 1)
				dimensions += ", ";
		}
		dimensions += ")";
		info[CL_DEVICE_MAX_WORK_ITEM_SIZES] = std::make_unique<OCL_Data>(CL_DEVICE_MAX_WORK_ITEM_SIZES, descriptions[CL_DEVICE_MAX_WORK_ITEM_SIZES], dimensions);

		//Print out everything
		std::ofstream output(output_filename);
		for (auto const &label : device_labels_ordered) {
			output << info[label]->pretty_print() << std::endl;
		}
		output.close();

		cl::Context context(devices);
		cl::CommandQueue queue =
			cl::CommandQueue(context, devices[device_number]);
		cl::Buffer bufferA = cl::Buffer(context,
										CL_MEM_WRITE_ONLY, 256 * sizeof(cl_ulong));
		std::ifstream sourceFile("kernel_00.cl");
		std::string sourceCode(
			std::istreambuf_iterator<char>(sourceFile), (std::istreambuf_iterator<char>()));
		cl::Program::Sources source(1,
									std::make_pair(sourceCode.c_str(),
												   sourceCode.length() + 1));

		cl::Program program = cl::Program(context, source);
		program.build(devices, "-cl-opt-disable");
		std::cout << "Built program!" << std::endl;

		cl::Kernel my_kernel(program, "clk_simple");
		my_kernel.setArg(0, bufferA);

		cl::NDRange global(256);
		cl::NDRange local(256);
		queue.enqueueNDRangeKernel(my_kernel,
								   cl::NullRange, global, local);
								   
		cl_ulong A[256];
		queue.enqueueReadBuffer(bufferA, CL_TRUE, 0,
								256 * sizeof(cl_ulong), A);
		for (int i = 0; i < 256; i++)
			std::cout << A[i] << " ";

		std::cout << "Microbenchmark #0 (device info) done." << std::endl;
	}
	catch (cl::Error error)	{
		std::cout << error.what() << "(" << error.err() << ")" << std::endl;
	}

	return 0;
}