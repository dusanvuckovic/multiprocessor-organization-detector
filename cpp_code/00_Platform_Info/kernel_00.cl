///define CL_CLOCK(reg) asm volatile("mov.u64 %0, %%clock64;" : "=l" (reg))


__kernel void clk_simple(__global ulong* A) {
	// Get the work-item�s unique ID
	int idx = get_global_id(0);
	/*ulong start;
	ulong end;
	CL_CLOCK(start);
	CL_CLOCK(end);*/
	asm("MOV RAX, RBX");
	A[idx] = idx;
}