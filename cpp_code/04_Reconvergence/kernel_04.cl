#define REPEAT64(S) S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S

#define CL_CLOCK(reg) asm volatile("mov.u64 %0, %%clock64;" : "=l" (reg))

__kernel void reconvergence(__global ulong* elapsed_time, __global uint* order) {
	ulong a = 5, b = 21;
	int idx = get_global_id(0);
	int position = order[idx];
	ulong start_time, stop_time;
	CL_CLOCK(start_time);	
	if (position == 0) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 1) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 2) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 3) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 4) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 5) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 6) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 7) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 8) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 9) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 10) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 11) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 12) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 13) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 14) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 15) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 16) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 17) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 18) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 19) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 20) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 21) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 22) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 23) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 24) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 25) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 26) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 27) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 28) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 29) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 30) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else if (position == 31) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	elapsed_time[idx] = stop_time - start_time;
}