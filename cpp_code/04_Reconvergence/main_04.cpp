#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <random>

/* This microbenchmark assumes reconvergence did happen and attempts to find the 
 * hardware order of serialization of diverging threads (FIFO, LIFO or other).
 * It runs a several work-items where every work-item has its own branch, 
 * taking clocks of the specific branches and comparing them in order to determine
 * which came first, and consequently infer the ordering. The output is the clock
 * differences for three specific orders (ascending, descending, and randomized).
 */

std::string output_filename = "../04_reconvergence.txt";

std::vector<cl_uint> order = {
	0, 1, 2, 3, 4, 5, 6, 7,
	8, 9, 10, 11, 12, 13, 14, 15,
	16, 17, 18, 19, 20, 21, 22, 23,
	24, 25, 26, 27, 28, 29, 30, 31
};

int platform_number = 0;
int device_number = 0;
unsigned int device_type = CL_DEVICE_TYPE_GPU;

std::vector<cl_uint> inverse = order;
std::vector<cl_uint> shuffle = order;

int N_ELEMENTS;

void initialize_order_arrays() {
	std::reverse(inverse.begin(), inverse.end());
	std::shuffle(shuffle.begin(), shuffle.end(), std::default_random_engine(14));
}

void get_results(cl::CommandQueue& queue, cl::Kernel& my_kernel, cl::Buffer& clockBuffer,
	cl::Buffer& orderBuffer, cl_ulong* clock, std::vector<cl_uint>& ordering) {

	queue.enqueueWriteBuffer(orderBuffer, CL_TRUE, 0,
		N_ELEMENTS * sizeof(cl_ulong), ordering.data());
	cl::NDRange global(N_ELEMENTS);
	cl::NDRange local(N_ELEMENTS);
	queue.enqueueNDRangeKernel(my_kernel,
		cl::NullRange, global, local);
	// Copy the output data back to the host
	queue.enqueueReadBuffer(clockBuffer, CL_TRUE, 0,
		N_ELEMENTS * sizeof(cl_ulong), clock);
}

void write_results(cl_ulong* clock, std::ofstream& file) {
	for (auto i = 0; i < N_ELEMENTS; i++) {
		file << clock[i] << " ";	
	}
	file << std::endl;	
}

int main() {
	initialize_order_arrays();
	try {
		std::vector<cl::Platform>platforms;
		cl::Platform::get(&platforms);

		std::vector<cl::Device>devices;
		platforms[platform_number].getDevices(device_type, &devices);

		cl::Context context(devices);
		cl::CommandQueue queue =
			cl::CommandQueue(context, devices[device_number]);
		
		std::ifstream sourceFile("kernel_04.cl");
		std::string sourceCode(
			std::istreambuf_iterator<char>(sourceFile),
			(std::istreambuf_iterator<char>()));
		cl::Program::Sources source(1,
			std::make_pair(sourceCode.c_str(),
				sourceCode.length() + 1));
		cl::Program program = cl::Program(context, source);
		program.build(devices, "-cl-opt-disable");

		cl::Kernel my_kernel(program, "reconvergence");
		// Cannot generalize as there is no way to dynamically modify kernel code.
		N_ELEMENTS = 32; //(cl_uint)my_kernel.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(devices[device_number]);

		cl_ulong* clock = new cl_ulong[N_ELEMENTS];
		cl::Buffer clockBuffer = cl::Buffer(context,
			CL_MEM_WRITE_ONLY, N_ELEMENTS * sizeof(cl_ulong));
		cl::Buffer orderBuffer = cl::Buffer(context,
			CL_MEM_READ_ONLY, N_ELEMENTS * sizeof(cl_ulong));

		my_kernel.setArg(0, clockBuffer);
		my_kernel.setArg(1, orderBuffer);

		std::ofstream output(output_filename);
		for (int i = 0; i < N_ELEMENTS; i++) {
			output << shuffle[i] << " ";
		}
		output << std::endl;

		get_results(queue, my_kernel, clockBuffer, orderBuffer, clock, order);
		write_results(clock, output);
		get_results(queue, my_kernel, clockBuffer, orderBuffer, clock, inverse);
		write_results(clock, output);
		get_results(queue, my_kernel, clockBuffer, orderBuffer, clock, shuffle);
		write_results(clock, output);

		delete[] clock;
		std::cout << "Microbenchmark #4 (reconvergence) done." << std::endl;
	}
	catch (cl::Error error) {
		std::cout << error.what() << "(" <<
			error.err() << ")" << std::endl;
	}
	return 0;
}