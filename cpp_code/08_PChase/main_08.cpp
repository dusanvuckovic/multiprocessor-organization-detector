#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

/* This microbenchmark measures access times in order to detect caching, using P-chase and 
   fine-grained P-chase mechanisms. For additional reference, see Saavedra 1993 and Mei 2015. 
   We run P-chase on global memory, local memory, constant memory, and the TLB 
   (very high strides for global memory), and run fine-grained P-chase on global and local
   memory. Output is a list of files, one for (test_type, memory_type) all of them
   containing tuples of (N, S, Taccess).
*/
   
constexpr cl_ulong KB = 1024;
constexpr cl_ulong MB = 1024 * 1024;

constexpr cl_uint platform_number = 0;
constexpr cl_uint device_number = 0;
constexpr cl_uint device_type = CL_DEVICE_TYPE_GPU;

constexpr cl_uint iterations = 1024;
constexpr cl_uint fake_iterations = iterations/4;

cl_ulong tvalues[iterations];
cl_uint indices[iterations];
cl::Buffer tvalues_buffer;
cl::Buffer indices_buffer;

cl::Program program;
std::vector<cl::Device> devices;

/* For a given array size and stride, construct an access pattern which will be copied
 * to the kernel, where it will be accessed.
 */
cl_uint* get_access_pattern(const cl_ulong access_size, const cl_ulong stride) {
	cl_uint* pattern = new cl_uint[access_size];
	for (cl_ulong i = 0; i < access_size; i++) {
		pattern[i] = (i + stride) % access_size;		
	}
	return pattern;
}

cl::Context context;
cl::CommandQueue queue;
cl::Buffer clock_buffer;

/* Run a P-chase test for N, s and a given type of memory. */
cl_double pchase_memory(const cl_ulong access_size, const cl_ulong stride, 
						cl::Kernel& kernel, bool is_local_kernel = false) {
	auto int_access_size = access_size / sizeof(cl_uint); // to ints
	cl_uint* access_pattern = get_access_pattern(int_access_size, stride);
	cl::Buffer access_buffer = cl::Buffer(context, CL_MEM_READ_ONLY, access_size);
	queue.enqueueWriteBuffer(access_buffer, CL_TRUE, 0, access_size, access_pattern);
	
	cl_ulong clock[1];

	// Set the kernel arguments	
	kernel.setArg(1, access_buffer);
	if (is_local_kernel) {
		kernel.setArg(4, int_access_size);
		kernel.setArg(5, access_size, NULL);
	}

	queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(1), cl::NDRange(1));
	queue.enqueueReadBuffer(clock_buffer, CL_TRUE, 0, sizeof(cl_ulong), clock);

	delete[] access_pattern;
	return (double)clock[0] / iterations;	
}

/* Run a fine-grained P-chase test for N, s and a given type of memory. */
void fine_grained_pchase_memory(const cl_ulong access_size, const cl_ulong stride, cl::Kernel& kernel) {
	auto int_access_size = access_size / sizeof(cl_uint); // to ints
	cl_uint* access_pattern = get_access_pattern(int_access_size, stride);

	cl::Buffer access_buffer = cl::Buffer(context, CL_MEM_READ_WRITE, access_size);
	queue.enqueueWriteBuffer(access_buffer, CL_TRUE, 0, access_size, access_pattern);

	kernel.setArg(0, access_buffer);
	kernel.setArg(1, iterations);
	kernel.setArg(2, tvalues_buffer);
	kernel.setArg(3, indices_buffer);
	kernel.setArg(4, sizeof(cl_ulong) * iterations, NULL);
	kernel.setArg(5, sizeof(cl_uint) * iterations, NULL);

	queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(1), cl::NDRange(1));
	queue.enqueueReadBuffer(tvalues_buffer, CL_TRUE, 0,
		sizeof(cl_ulong)*iterations, tvalues);
	queue.enqueueReadBuffer(indices_buffer, CL_TRUE, 0,
		sizeof(cl_uint) * iterations, indices);

	delete[] access_pattern;
}

/* Run a batch of P-chase tests for a given kernel and write out the results in a file. */
void pchase(cl::Kernel& kernel, const std::string output_filename, cl_ulong start, cl_ulong increment, cl_ulong stop,
			bool report = true, std::vector<cl_ulong> strides = { 1, 2, 4, 8, 16, 32, 48, 64, 128, 256 }, 
			bool is_local_kernel = false) {

	std::ofstream output_total(output_filename);
	cl_ulong iterated = 0;
	cl_ulong iterations = (stop - start) / increment;
	for (cl_ulong stride : strides) {
		for (cl_ulong size = start; size < stop; size += increment) {
			auto k = pchase_memory(size, stride, kernel, is_local_kernel);
			output_total << size << " B\t" << stride << "\t" << k << std::endl;
			if (report) {
				iterated++;
				std::cout << "Iterated " << iterated << "/" << iterations << " for stride " << stride << "." << std::endl;
			}
		}
		iterated = 0;
	}
}

/* Run batches of P-chase measurements for all the memory types in the system. */
void pchase_all_measurements(std::vector<cl::Device>& devices) {
	const std::string output_global("../r_08_pchase_global.txt");
	const std::string output_constant("../r_08_pchase_constant.txt");
	const std::string output_local("../r_08_pchase_local.txt");
	const std::string output_TLB("../r_08_pchase_TLB.txt");

	// Make the global kernel
	cl::Kernel global_kernel = cl::Kernel(program, "pchase_global");
	global_kernel.setArg(0, clock_buffer);
	global_kernel.setArg(2, iterations);
	global_kernel.setArg(3, fake_iterations);

	// Make the constant kernel
	cl::Kernel constant_kernel = cl::Kernel(program, "pchase_constant");
	constant_kernel.setArg(0, clock_buffer);
	constant_kernel.setArg(2, iterations);
	constant_kernel.setArg(3, fake_iterations);

	// Make the local kernel
	cl::Kernel local_kernel = cl::Kernel(program, "pchase_local");
	local_kernel.setArg(0, clock_buffer);
	local_kernel.setArg(2, iterations);
	local_kernel.setArg(3, fake_iterations);

	// Get global memory data.
	cl_ulong max_cache_size = 20 * MB;
	pchase(global_kernel, output_global, 256, 256, max_cache_size);

	// Get constant memory data.
	cl_ulong constant_memory_size = devices[device_number].getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>();
	pchase(constant_kernel, output_constant, 256, 256, constant_memory_size);

	// Get local memory data.
	cl_ulong local_memory_size = devices[device_number].getInfo<CL_DEVICE_LOCAL_MEM_SIZE>();
	pchase(local_kernel, output_local, 256, 256, local_memory_size, true, {1, 2, 4, 8, 16, 32, 48, 64, 128, 256}, true);

	// Get TLB data.	
	pchase(global_kernel, output_TLB, 1 * MB, 256 * KB, 300 * MB, true, {1*MB, 2*MB, 4*MB, 8*MB});
}

/* Run a batch of fine-grained P-chase tests for a given kernel and write out the results in a file. */
void fine_grained_pchase(cl::Kernel& kernel, const std::string output_filename, cl_ulong start, 
						 cl_ulong increment, cl_ulong stop, bool report = false, 
						 std::vector<cl_ulong> strides = { 1, 2, 4, 8, 16, 32, 48, 64, 128, 256 }) {

	std::ofstream output(output_filename);
	cl_ulong iterated = 0;
	cl_ulong iterations = (stop - start) / increment;
	for (cl_ulong stride : strides) {
		for (cl_ulong size = start; size < stop; size += increment) {
			fine_grained_pchase_memory(size, stride, kernel);
			output << size << "\t" << stride << std::endl;
			for (int i = 0; i < iterations; i++) {
				output << tvalues[i] << " ";
			}
			output << std::endl;
			for (int i = 0; i < iterations; i++) {
				output << indices[i] << " ";
			}
			output << std::endl;
			if (report) {
				iterated++;
				std::cout << "Iterated " << iterated << "/" << iterations << " for stride " << stride << "." << std::endl;
			}			
		}
		iterated = 0;
	}
}

/* Run batches of P-chase measurements for all the memory types in the system. */
void fine_grained_pchase_all_measurements(cl::Program& program, std::vector<cl::Device>& devices) {
	cl::Kernel fg_global_kernel = cl::Kernel(program, "fine_grained_pchase_global");	
	cl::Kernel fg_constant_kernel = cl::Kernel(program, "fine_grained_pchase_constant");
	cl_ulong constant_memory_size = devices[device_number].getInfo<CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE>();
	cl_ulong local_memory_size = devices[device_number].getInfo<CL_DEVICE_LOCAL_MEM_SIZE>();

	const std::string output_global = "../r_08_fg_global.txt";
	const std::string output_constant = "../r_08_fg_constant.txt";
	
	fine_grained_pchase(fg_global_kernel, output_global, 256, 
						sizeof(cl_uint)*16, local_memory_size);
	fine_grained_pchase(fg_constant_kernel, output_constant, 256, 
						sizeof(cl_uint)*16, std::min(constant_memory_size, local_memory_size));	
}

int main() {
	try {
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);

		platforms[platform_number].getDevices(device_type, &devices);

		context = cl::Context(devices);

		queue = cl::CommandQueue(context, devices[device_number]);

		std::ifstream sourceFile("kernel_08.cl");
		std::string sourceCode(
			std::istreambuf_iterator<char>(sourceFile),
			(std::istreambuf_iterator<char>()));
		cl::Program::Sources source(1,
			std::make_pair(sourceCode.c_str(),
				sourceCode.length() + 1));
		clock_buffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_ulong));
		tvalues_buffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_ulong)*iterations);
		indices_buffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_uint)*iterations);

		program = cl::Program(context, source);
		program.build(devices, "-cl-opt-disable");		
		
		pchase_all_measurements(program, devices);
		fine_grained_pchase_all_measurements(program, devices);
	
	catch (cl::Error error) {
		std::cout << error.what() << "(" <<
			error.err() << ")" << std::endl;
	}
	return 0;
}