#define CL_CLOCK(reg) asm volatile("mov.u64 %0, %%clock64;" : "=l" (reg))


__kernel void pchase_global(__global ulong* elapsed_time, __global uint* A, uint iterations, uint fake_iterations) {
	ulong start_time, stop_time;
	int idx = get_global_id(0);
	uint acc = 0;
	for (uint i = 0; i < fake_iterations; i++) {
		acc = A[acc];
	}
	CL_CLOCK(start_time);
	for (uint i = 0; i < iterations; i++) {
		acc = A[acc];
	}
	CL_CLOCK(stop_time);
	elapsed_time[idx] = stop_time - start_time;
}

__kernel void pchase_constant(__global ulong* elapsed_time, __constant uint* A, uint iterations, uint fake_iterations) {
	ulong start_time, stop_time;
	int idx = get_global_id(0);
	uint acc = 0;
	for (uint i = 0; i < fake_iterations; i++) {
		acc = A[acc];
	}
	CL_CLOCK(start_time);	
	for (uint i = 0; i < iterations; i++) {
		acc = A[acc];
	}
	CL_CLOCK(stop_time);	
	elapsed_time[idx] = stop_time - start_time;
}

__kernel void pchase_local(__global ulong* elapsed_time, __global uint* A, uint iterations, 
						   uint fake_iterations, ulong N, local uint* A_local) {
	for (ulong i = 0; i < N; i++) {
		A_local[i] = A[i];
	}
	ulong start_time, stop_time;
	int idx = get_global_id(0);
	uint acc = 0;
	for (uint i = 0; i < fake_iterations; i++) {
		acc = A_local[acc];
	}
	CL_CLOCK(start_time);	
	for (uint i = 0; i < iterations; i++) {
		acc = A_local[acc];
	}
	CL_CLOCK(stop_time);	
	elapsed_time[idx] = stop_time - start_time;
}

__kernel void fine_grained_pchase_global(__global uint* A, uint iterations, __global ulong* tvalues, 
										 __global uint* indices, __local ulong* l_tvalues, 
										 __local uint* l_indices) {
	ulong start_time, stop_time;
	uint acc = 0;	
	for (uint it = 0; it < iterations / 4; it++)
		acc = A[acc];
	for (uint it = 0; it < iterations; it++) {	
		CL_CLOCK(start_time);
		acc = A[acc];
		l_indices[it] = acc;
		CL_CLOCK(stop_time);
		l_tvalues[it] = stop_time - start_time;
	} 
	for (uint it = 0; it < iterations; it++) {
		tvalues[it] = l_tvalues[it];
		indices[it] = l_indices[it];
	}
}

__kernel void fine_grained_pchase_constant(__constant uint* A, uint iterations, __global ulong* tvalues, 
										   __global uint* indices, __local ulong* l_tvalues, 
										   __local uint* l_indices) {
	ulong start_time, stop_time;
	uint acc = 0;
	for (uint it = 0; it < iterations / 4; it++)
		acc = A[acc];
	for (uint it = 0; it < iterations; it++) {
		CL_CLOCK(start_time);
		acc = A[acc];
		l_indices[it] = acc;
		CL_CLOCK(stop_time);		
		l_tvalues[it] = stop_time - start_time;
	}
	for (uint it = 0; it < iterations; it++) {
		tvalues[it] = l_tvalues[it];
		indices[it] = l_indices[it];
	}
}