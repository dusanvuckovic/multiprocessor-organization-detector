#define CL_CLOCK(reg) asm volatile("mov.u64 %0, %%clock64;" : "=l" (reg))

#define REPEAT64(S) S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S \
					S S S S S S S S

inline ulong cl_clock() {
	ulong clk;
	asm volatile("mov.u64 %0, %%clock64;" : "=l" (clk));
	return clk;
}

__kernel void clk(__global ulong* elapsed_time) {
	int idx = get_global_id(0);
	ulong start_time;
	ulong stop_time;
	CL_CLOCK(start_time);
	CL_CLOCK(stop_time);		
	elapsed_time[idx] = stop_time - start_time;	
}

__kernel void launch(__global ulong* starts) {
	int idx = get_global_id(0);
	ulong val1 = 1, val2 = 2;
	ulong start_time;
	
	CL_CLOCK(start_time);
	/* Some work is necessary in order to differentiate between
	the different work-groups. */
	val1 += val2; val2 += val1;
	val1 += val2; val2 += val1;
	val1 += val2; val2 += val1;
	val1 += val2; val2 += val1;
	starts[idx] = start_time;
}
