#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdint>
#include <map>


int platform_number = 0;
int device_number = 0;
unsigned int device_type = CL_DEVICE_TYPE_GPU;

/* This microbenchmark accesses the system clock. It runs the maximum possible number 
 * of threads per OpenCL dimension. Its output is the array of the differences between
 * two consecutive calls of the system clock - one difference for every thread. It's written
 * into "clock.txt". Upon viewing "clock.txt", we can conclude whether various "compute units"
 * (what Nvidia calls "streaming multiprocessors") of the OpenCL device share a system clock or not,
 * based on whether the clock values are all the same or not. We can also, assuming the clock isn't
 * unique, independently confirm the number of work-items mapped to work-groups (threads per warp).
*/

const std::string output_filename_clock = "../01_clock.txt";
const std::string output_filename_launch = "../01_launch.txt";

// Reads the parameters from MB 00 and returns a map
// of them.
std::map<std::string, std::string> get_parameters() {
	std::ifstream input("../device_properties.txt");
	std::map<std::string, std::string> data;
	std::string line;
	while (std::getline(input, line)) {
		std::string key = line.substr(0, line.find(": "));
		std::string value = line.substr(line.find(": ") + 2);
		data[key] = value;
	}
	return data;
}

void get_clock_differentials(cl::Program& program, cl::Context& context, cl::CommandQueue& queue) {

	/* Get necessary data, varying by device, necessary for running the test. */
	auto parameters = get_parameters();
	const uint64_t num_of_CUs = std::stoi(parameters["Number of parallel compute units"]);
	const uint64_t work_items_per_work_group = std::stoi(parameters["Maximum number of work-items in a work-group"]);
	const uint64_t N_ELEMENTS = num_of_CUs * work_items_per_work_group;


	uint64_t* A = new uint64_t[N_ELEMENTS];
	for (auto i = 0; i < N_ELEMENTS; i++) {
		A[i] = 0;
	}
	cl::Buffer bufferA = cl::Buffer(context,
		CL_MEM_WRITE_ONLY, N_ELEMENTS * sizeof(uint64_t));

	cl::Kernel my_kernel(program, "clk");
	my_kernel.setArg(0, bufferA);


	queue.enqueueNDRangeKernel(my_kernel,
		cl::NullRange, cl::NDRange(N_ELEMENTS), cl::NDRange(work_items_per_work_group));
	queue.enqueueReadBuffer(bufferA, CL_TRUE, 0,
		N_ELEMENTS * sizeof(uint64_t), A);


	std::ofstream output(output_filename_clock);
	for (auto i = 0; i < num_of_CUs; i++) {
		for (auto j = 0; j < work_items_per_work_group; j++) {
			output << A[i * num_of_CUs + j] << " ";
		}
		output << std::endl;
	}
	output.close();
	delete[] A;
}

void get_launch_differentials(cl::Program& program, cl::Context& context, cl::CommandQueue& queue) {

	const uint64_t GROUP1 = 10, GROUP2 = 30;

	uint64_t* starts = new uint64_t[GROUP2];
	for (auto i = 0; i < GROUP2; i++) {
		starts[i] = 15;
	}

	cl::Buffer bufferStarts = cl::Buffer(context,
		CL_MEM_WRITE_ONLY, GROUP2 * sizeof(uint64_t));

	cl::Kernel my_kernel(program, "launch");
	my_kernel.setArg(0, bufferStarts);

	queue.enqueueNDRangeKernel(my_kernel,
		cl::NullRange, cl::NDRange(GROUP1), cl::NDRange(1));
	queue.enqueueReadBuffer(bufferStarts, CL_TRUE, 0,
		GROUP1 * sizeof(uint64_t), starts);

	std::ofstream output(output_filename_launch);
	for (int i = 0; i < GROUP1; i++) {
		output << starts[i] << " ";
	}
	output << std::endl;
	
	queue.enqueueNDRangeKernel(my_kernel,
		cl::NullRange, cl::NDRange(GROUP2), cl::NDRange(1));
	queue.enqueueReadBuffer(bufferStarts, CL_TRUE, 0,
		GROUP2 * sizeof(uint64_t), starts);

	for (int i = 0; i < GROUP2; i++) {
		output << starts[i] << " ";
	}
	

	output.close();
	delete[] starts;
}


int main() {

	try {
		
		std::vector<cl::Platform> platforms;
		cl::Platform::get(&platforms);

		std::vector<cl::Device> devices;
		platforms[platform_number].getDevices(device_type, &devices);

		cl::Context context(devices);
		cl::CommandQueue queue =
			cl::CommandQueue(context, devices[device_number]);
		
		std::ifstream sourceFile("kernel_01.cl");
		std::string sourceCode(
			std::istreambuf_iterator<char>(sourceFile), (std::istreambuf_iterator<char>()));
		cl::Program::Sources source(1,
			std::make_pair(sourceCode.c_str(),
				sourceCode.length() + 1));
		cl::Program program = cl::Program(context, source);
		program.build(devices, "-cl-opt-disable");
		
		get_clock_differentials(program, context, queue);
		get_launch_differentials(program, context, queue);

		std::cout << "Microbenchmark #1 (clock) done." << std::endl;
	}	
	catch (cl::Error error)	{
		std::cout << error.what() << "(" <<
			error.err() << ")" << std::endl;
	}

	return 0;
}