#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <iostream>
#include <fstream>
#include <string>

std::string output_filename = "../03_divergence.txt";

/* This microbenchmark tries seeing whether the device diverges when processing
 * conditional blocks (as GPUs commonly do) or not (as CPUs usually don't do). 
 * It runs a several work-items and splits a work-group into half, noting the 
 * clock of the specific groups, which is the output. From the output we conclude
 * whether divergence exists or not.
 */

int platform_number = 0;
int device_number = 0;
unsigned int device_type = CL_DEVICE_TYPE_GPU;


cl_ulong* get_clock_array(const cl_ulong num_elements) {
	cl_ulong* clock = new cl_ulong[num_elements];
	for (auto i = 0; i < num_elements; i++) {
		clock[i] = 0;
	}
	return clock;
}

int main() {
	try {
		std::vector<cl::Platform>platforms;
		cl::Platform::get(&platforms);

		std::vector<cl::Device>devices;
		platforms[platform_number].getDevices(device_type, &devices);

		cl::Context context(devices);
		cl::CommandQueue queue =
			cl::CommandQueue(context, devices[device_number]);

		std::ifstream sourceFile("kernel_03.cl");
		std::string sourceCode(
			std::istreambuf_iterator<char>(sourceFile),
			(std::istreambuf_iterator<char>()));
		cl::Program::Sources source(1,
			std::make_pair(sourceCode.c_str(),
				sourceCode.length() + 1));
		cl::Program program = cl::Program(context, source);
		program.build(devices, "-cl-opt-disable");

		cl_ulong size = my_kernel.getWorkGroupInfo<CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE>(devices[0]);
		cl::Buffer bufferClock = cl::Buffer(context,
			CL_MEM_WRITE_ONLY, size * sizeof(cl_ulong));

		cl::Kernel my_kernel(program, "divergence");
		my_kernel.setArg(0, bufferClock);
		my_kernel.setArg(1, size/2);

		cl::NDRange global(size);
		cl::NDRange local(size);
		queue.enqueueNDRangeKernel(my_kernel,
			cl::NullRange, global, local);

		cl_ulong* clock = get_clock_array(size);
		queue.enqueueReadBuffer(bufferClock, CL_TRUE, 0,
			size * sizeof(cl_ulong), clock);
		std::ofstream file(output_filename);		
		for (int i = 0; i < size; i++) {
			file << clock[i] << " ";
		}
		file.close();
		delete[] clock;
		std::cout << "Microbenchmark #3 (divergence) done." << std::endl;

	}
	catch (cl::Error error) {
		std::cout << error.what() << "(" <<
			error.err() << ")" << std::endl;
	}
	return 0;
}