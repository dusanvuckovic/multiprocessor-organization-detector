#define REPEAT64(S) S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S S
#define CL_CLOCK(reg) asm volatile("mov.u64 %0, %%clock64;" : "=l" (reg))

__kernel void divergence(__global ulong* elapsed_time, __private ulong size) {
	int idx = get_global_id(0);
	ulong a = 7, b = 12;
	ulong start_time, stop_time;
	CL_CLOCK(start_time);
	if (idx < size) {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	else {
		REPEAT64(a += b; b += a;)
		CL_CLOCK(stop_time);
	}
	elapsed_time[idx] = stop_time - start_time;
}