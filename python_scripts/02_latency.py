import os.path
from operator import itemgetter
from typing import List
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from matplotlib import rcParams

english = False

OPERATIONS = 'Operation' if english else "Operacija"
OPERATIONS_MS = 'Operations/ms' if english else "Operacije/ms"

OPERATION_MS_TYPE = 'Operation/ms ({0})' if english else "Operacije/ms ({0})"
OPERATIONS_MS = 'Operations/ms' if english else "Operacije/ms"
OPERATIONS_TYPE = 'Operation ({0})' if english else "Operacija ({0})"

LATENCY_TITLE = "Latency" if english else "Kašnenje"
THROUGHPUT_TITLE = "Throughput" if english else "Protok"
TYPE_COMPARISON_TITLE = "Throughput comparison of type" if english else "Poređenje protoka po tipu"
THROUGHPUT_TYPE_TITLE = "Throughput of type" if english else "Protok po tipu"

LATENCY_MS = 'Latency (ms)' if english else "Kašnjenje (ms)"
TITLE = "AMD CPU"

TYPE = 'Type' if english else "Tip"

input_filename = "../immediate_results/02_latency_and_throughput.txt"
output_folder = "../meaningful_results/r_02/"

output_folder_latencies = "r_02_latencies/"
output_folder_throughput = "r_02_throughput/"
output_folder_operation_comparison = "r_02_operation_comparison/"
output_folder_type_comparison = "r_02_type_comparison/"

"""
A script processing the latency and throughput microbenchmark results.
Returns a set of graphs for comparison of multiprocessor performance
characteristics.
"""


class Measurement:
    def __init__(self, line: List[str]):
        self.operation_name = line[0].lower()
        self.type = line[1].lower()
        self.iterations = int(line[2])
        self.repetitions = int(line[3])
        self.latency_ms = float(line[4])
        self.has_clock = (len(line) == 8)
        if self.has_clock:
            self.latency_clocks = float(line[5])
            self.operation_ms = float(line[6])
            self.operation_clocks = float(line[7])
        else:
            self.operation_ms = float(line[5])

    def __repr__(self):
        if self.has_clock:
            return "({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7})".format(self.operation_name, self.type, self.iterations,
                                                                     self.repetitions, self.latency_ms,
                                                                     self.latency_clocks,
                                                                     self.operation_ms, self.operation_clocks)
        else:
            return "({0}, {1}, {2}, {3}, {4}, {5})".format(self.operation_name, self.type, self.iterations,
                                                           self.repetitions, self.latency_ms,
                                                           self.operation_ms)


def __get_measurements(input_filename: str) -> List[Measurement]:
    f = open(input_filename, 'r')
    lines = f.readlines()[1:]
    lines = [line.rstrip()[1:-2] for line in lines]
    lines = [ln.split(', ') for ln in lines]
    measurements = [Measurement(line) for line in lines]
    return measurements


def process_02_measurements(input_filename: str):
    rcParams.update({'figure.autolayout': True})
    if not os.path.isfile(input_filename):
        print("No file in " + input_filename)
        return

    measurements = __get_measurements(input_filename)

    for type in ('uint', 'int', 'long', 'ulong', 'float', 'double'):
        create_latency_plot(measurements, type)
        create_throughput_plot(measurements, type)
    for operation in ('add', 'sub', 'min', 'mul', 'div', 'mad'):
        create_operation_comparison_plot(measurements, operation)
    create_type_comparison_plot(measurements, 'float', 'double')
    create_type_comparison_plot(measurements, 'uint', 'ulong')


def create_latency_plot(measurements: List[Measurement], type: str):
    plt.rcdefaults()
    plt.style.use('fivethirtyeight')
    filtered_measurements = list(filter(lambda measurement: measurement.type == type, measurements))
    data = [(measurement.operation_name, measurement.latency_ms) for measurement in filtered_measurements]
    data.sort(key=itemgetter(1), reverse=True)
    operations = [x[0] for x in data]
    latencies = [x[1] for x in data]
    fig, ax = plt.subplots()
    plt.barh(y=operations, width=latencies)

    print(output_folder_latencies + type + ".png")
    plt.xlabel(LATENCY_MS)
    plt.ylabel(OPERATIONS_TYPE.format(type))
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(10)
    plt.title(LATENCY_TITLE + ", " + TITLE)
    plt.savefig(output_folder + output_folder_latencies + type + ".png", bbox_inches="tight", format='png', dpi=1000)


def create_throughput_plot(measurements: List[Measurement], type: str):
    plt.rcdefaults()
    plt.style.use('fivethirtyeight')
    filtered_measurements = list(filter(lambda measurement: measurement.type == type, measurements))
    data = [(measurement.operation_name, measurement.operation_ms) for measurement in filtered_measurements]
    data.sort(key=itemgetter(1))
    operations = [x[0] for x in data]
    latencies = [x[1] for x in data]
    fig, ax = plt.subplots()
    plt.barh(y=operations, width=latencies)
    # for i, v in enumerate(latencies):
    #   ax.text(v, i - 0.25, str(v), color='black', fontsize=12)
    print(output_folder_throughput + type + ".png")
    plt.xlabel(OPERATIONS_MS)
    plt.ylabel(OPERATIONS_TYPE.format(type))
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(10)
    plt.title(THROUGHPUT_TITLE + ", " + TITLE)
    plt.savefig(output_folder + output_folder_throughput + type + ".png", bbox_inches="tight", format='png', dpi=1000)


def create_operation_comparison_plot(measurements: List[Measurement], operation: str):
    plt.rcdefaults()
    plt.style.use('fivethirtyeight')
    filtered_measurements = list(filter(lambda measurement: measurement.operation_name == operation, measurements))
    data = [(measurement.type, measurement.operation_ms) for measurement in filtered_measurements]
    data.sort(key=itemgetter(1))
    types = [x[0] for x in data]
    latencies = [x[1] for x in data]
    fig, ax = plt.subplots()
    plt.barh(y=types, width=latencies)
    print(output_folder_operation_comparison + operation + ".png")
    plt.xlabel(OPERATION_MS_TYPE.format(operation))
    plt.ylabel(TYPE)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(10)
    plt.title(THROUGHPUT_TYPE_TITLE + ", " + TITLE)
    plt.savefig(output_folder + output_folder_operation_comparison + operation + ".png", 
                bbox_inches="tight", format='png', dpi=1000)


def create_type_comparison_plot(measurements: List[Measurement], type1: str, type2: str):

    plt.rcdefaults()
    plt.style.use('fivethirtyeight')
    operations1 = {measurement.operation_name for measurement in measurements if measurement.type == type1}
    operations2 = {measurement.operation_name for measurement in measurements if measurement.type == type2}
    operations = operations1 & operations2  # set intersection
    throughputs1 = {measurement.operation_name: measurement.operation_ms for measurement in measurements
                    if measurement.type == type1 and measurement.operation_name in operations}
    throughputs2 = {measurement.operation_name: measurement.operation_ms for measurement in measurements
                    if measurement.type == type2 and measurement.operation_name in operations}
    df = pd.DataFrame({type1: throughputs1, type2: throughputs2})
    df.sort_values(by=type1, inplace=True)
    a_vals = df[type1]
    b_vals = df[type2]
    ind = np.arange(df.shape[0])
    width = 0.35

    fig, ax = plt.subplots()
    a = ax.barh(ind, a_vals, width, edgecolor='black', linewidth=1.1)  # plot a vals
    b = ax.barh(ind + width, b_vals, width)  # plot b vals
    ax.set_yticks(ind + width)  # position axis ticks
    ax.set_yticklabels(df.index, fontsize="x-small")  # set them to the names
    ax.legend((a[0], b[0]), [type1, type2], loc='best', fontsize='x-small')
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize('x-small')

    plt.xlabel(OPERATIONS_MS, fontsize=12)
    plt.ylabel(OPERATIONS, fontsize=12)
    plt.title(TYPE_COMPARISON_TITLE + ", " + TITLE)
    print(output_folder_type_comparison + type1 + "_v_" + type2 + ".png")
    plt.savefig(output_folder + output_folder_type_comparison + type1 + "_v_" + type2 + ".png", 
                bbox_inches="tight", format='png', dpi=1000)


def optional_make_dir(path: str):
    if not os.path.isdir(path):
        os.mkdir(path)


if __name__ == '__main__':
    if not os.path.isfile(input_filename):
        print("No latency and throughput file!")
    else:
        paths = [output_folder, output_folder + output_folder_latencies, output_folder + output_folder_throughput,
                 output_folder + output_folder_operation_comparison, output_folder + output_folder_type_comparison]
        for path in paths:
            optional_make_dir(path)
        process_02_measurements(input_filename)
