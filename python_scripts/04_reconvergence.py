import os
from enum import Enum
from operator import itemgetter
from typing import List

input_filename = "../immediate_results/04_reconvergence.txt"
output_filename = "../meaningful_results/r_04_reconvergence.txt"

"""
A script processing the reconvergence microbenchmark.
Returns a text file containing:
    1) serialization unit of ascending, descending and random divergences
    2) if the random divergence's serialization unit is not a stack or a queue,
    returns the original order and the executed order
"""

class Ordering(Enum):
    FIFO = "stack"
    LIFO = "queue"
    OTHER = "not stack nor queue"


def __is_ascending(list: List[int]) -> bool:
    for k in range(len(list) - 1):
        if list[k] > list[k + 1]:
            return False
    return True


def __is_descending(list: List[int]) -> bool:
    for k in range(len(list) - 1):
        if list[k] < list[k + 1]:
            return False
    return True


def get_ordering(list: List[int]) -> Ordering:
    if __is_ascending(list):
        return Ordering.LIFO
    elif __is_descending(list):
        return Ordering.FIFO
    else:
        return Ordering.OTHER


def matches_random(random_accesses: List[int], random_order: List[int]) -> Ordering:
    random_order = list(enumerate(random_order))
    random_order = [x[0] for x in list(sorted(random_order, key=itemgetter(1)))]
    if random_order == random_accesses:
        return Ordering.LIFO
    elif random_order == list(reversed(random_accesses)):
        return Ordering.FIFO
    else:
        return Ordering.OTHER


def reconvergence(input_filename: str):
    if not os.path.isfile(input_filename):
        print("No input file at {}!".format(input_filename))
        return

    lines = open(input_filename).readlines()
    random_accesses = [int(x) for x in lines[0].strip().split(' ')]
    ascending_order = [int(x) for x in lines[1].strip().split(' ')]
    descending_order = [int(x) for x in lines[2].strip().split(' ')]
    random_order = [int(x) for x in lines[3].strip().split(' ')]

    ordering_asc = get_ordering(ascending_order)
    ordering_dsc = get_ordering(list(reversed(descending_order)))
    ordering_rnd = matches_random(random_accesses, random_order)

    output = open(output_filename, mode='w')
    output.write("Specified orders: {0} for ascending, {1} for descending, {2} for random.\n"
                 .format(ordering_asc.value, ordering_dsc.value, ordering_rnd.value))
    if ordering_rnd == Ordering.OTHER or ordering_asc == Ordering.OTHER or ordering_dsc == Ordering.OTHER:
        output.write("Original order: " + ", ".join(map(str, random_accesses)) + "\n")
        vals = [val[0] for val in sorted((enumerate(random_order)), key=itemgetter(1))]
        output.write("Executed order: " + ", ".join(map(str, vals)) + "\n")


if __name__ == '__main__':
    reconvergence(input_filename)
