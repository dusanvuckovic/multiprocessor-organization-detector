import itertools
import os.path
from collections import Counter
from typing import List, Tuple

"""
A script processing the system clock microbenchmark.
Returns a text file containing:
    1) the lowest value of register transfer, in clocks
    2) whether the clock is per-execution unit or global
    3) if per-execution, lowest granularity    
    4) where it fits in Wong's granularity levels        
"""

input_filename_clock = r"../immediate_results/01_clock.txt"
input_filename_timings = r"../immediate_results/01_launch.txt"
output_filename = r"../meaningful_results/r_01_clock.txt"


def __get_values(input_filename: str) -> List[List[int]]:
    """
    :return: list of compute unit data, each of which is a list of the thread accesses
    """
    f = open(input_filename, 'r')
    return [[int(x) for x in line.strip().split(" ")] for line in f.readlines()]


def __lowest_value(data: List[List[int]]) -> int:
    return min([min(CU) for CU in data])


def __is_global(data: List[List[int]]) -> bool:
    total_list = list(itertools.chain(*data))
    c = Counter(total_list)
    return len(c) == 1


def __lowest_granularity(data: List[List[int]]) -> int:
    min_granularity = len(data[0])
    for CU in range(len(data)):
        current_granularity = 1
        current_item = data[CU][0]
        for x in data[CU][1:]:
            if current_item == x:
                current_granularity += 1
            else:
                min_granularity = min(min_granularity, current_granularity)
                current_granularity = 1
                current_item = x

    return min_granularity


def __get_granularities(values: List[List[int]]) -> Tuple[int, int, int]:
    lowest_value = __lowest_value(values)
    is_global = __is_global(values)
    granularity = __lowest_granularity(values)
    return lowest_value, is_global, granularity


def __get_wong_criterion(values: List[List[int]]) -> Tuple[int, int, int]:
    val1 = values[0]
    val1_uniques = set(val1)
    if len(val1_uniques) == 1:
        return "global"
    elif len(val1_uniques) == len(val1):
        return "per execution unit"
    else:
        val2 = values[1]
        val2_uniques = set(val2)
        if len(val2_uniques) == 1:
            return "global"
        elif len(val2_uniques) == len(val2):
            return "per execution unit"
        else:
            return "per compute unit"


def process_01_clock():
    if not os.path.isfile(input_filename_clock):
        print("No file in " + input_filename_clock)
        return
    if not os.path.isfile(input_filename_timings):
        print("No file in " + input_filename_timings)
        return

    timings = __get_values(input_filename_clock)
    launches = __get_values(input_filename_timings)

    lowest_value, is_global, granularity = __get_granularities(timings)
    wong_criterion = __get_wong_criterion(timings)

    output = open(output_filename, mode='w')
    output.write("Lowest value of register transfer is {} clocks.\n".format(lowest_value))
    output.write("System clock: " + ("unique" if is_global else "multiple clocks in device" + ".\n"))
    if not is_global:
        output.write("System clock granularity is no higher than {} consecutive values.\n".format(granularity))
    output.write("Wong criterion states the system clock is {}.".format(wong_criterion))


if __name__ == '__main__':
    process_01_clock()
