import os.path
from typing import List
import matplotlib.pyplot as plt
from matplotlib import rcParams

english = False

"""
A script processing the pinned memory microbenchmark results.
Returns a graph denoting the memory access time results.
"""

TIME_NEEDED_PER_ACCESSES_MS = 'Time needed per 2048 accesses (ms)' if english \
                         else "Vreme za 2048 pristupa memoriji (ms)"
NUMBER_OF_THREADS = 'Number of threads' if english else "Broj niti"

GLOBAL_MEM = "GLOBAL"
LOCAL_MEM = "LOCAL"
CONSTANT_MEM = "CONSTANT"

LEGEND_TITLE = "Broadcasting degree" if english else "Stepen difuzije"

TEST1 = "TEST1"
TEST2 = "TEST2"
TEST3 = "TEST3"

DEVICE = "AMD Radeon RX Vega 56"

input_filename = "../immediate_results/07_broadcasting_parallel_access.txt"
output_filename = "../meaningful_results/r_07/"


class Measurement:
    # (MEM_TYPE, TEST_NUM, THREAD_NUM, BROADCAST_DEGREE, MS, CLOCKS/ITERATION)
    def __init__(self, line: List[str]):
        self.memory_type = line[0]
        self.test = line[1]
        self.number_of_threads = int(line[2])
        self.broadcast_degree = int(line[3])
        self.ms = float(line[4])
        self.has_clock = len(line) == 6
        if self.has_clock:
            self.clocks_per_iteration = float(line[5])

    def __repr__(self):
        if self.has_clock:
            return "({0}, {1}, {2}, {3}, {4}, {5})".format(self.memory_type, self.test, self.number_of_threads,
                                                           self.broadcast_degree, self.ms, self.clocks_per_iteration)
        else:
            return "({0}, {1}, {2}, {3}, {4})".format(self.memory_type, self.test, self.number_of_threads,
                                                      self.broadcast_degree, self.ms)


def __get_measurements(input_filename: str) -> List[Measurement]:
    f = open(input_filename, 'r')
    lines = f.readlines()
    lines = [line.rstrip()[1:-2] for line in lines]
    lines = [ln.split(', ') for ln in lines]
    measurements = [Measurement(line) for line in lines]
    return measurements


def process_07_broadcast_parallel_access(input_filename: str):
    rcParams.update({'figure.autolayout': True})
    if not os.path.isfile(input_filename):
        print("No file in " + input_filename)
        return
    measurements = __get_measurements(input_filename)
    for memory_type in [GLOBAL_MEM, LOCAL_MEM, CONSTANT_MEM]:
        for test in [TEST1, TEST2, TEST3]:
            make_plot(measurements, test, memory_type)
    output = open("{}memory_data.txt".format(output_filename), 'w')
    for memory_type in [GLOBAL_MEM, LOCAL_MEM, CONSTANT_MEM]:
        res = memory_type + "\n"
        res += broadcast_parallel(measurements, memory_type)
        res += aligned_consecutive(measurements, memory_type)
        output.write(res + "\n")


def text_test_mem_type(test: str, memory_type: str) -> str:
    test_nums = {TEST1: "1", TEST2: "2", TEST3: "3"}

    memory_types = {GLOBAL_MEM: "global", LOCAL_MEM: "local", CONSTANT_MEM: "constant"} if english else \
                   {GLOBAL_MEM: "globalne", LOCAL_MEM: "lokalne", CONSTANT_MEM: "konstantne"}
    return test_nums[test] + " " + memory_types[memory_type]


def make_plot(measurements: List[Measurement], test: str, memory_type: str):
    plt.clf()
    measurements_plot = [measurement for measurement in measurements
                         if measurement.test == test and measurement.memory_type == memory_type]
    broadcasting_degrees = sorted({measurement.broadcast_degree for measurement in measurements_plot})
    numbers_of_threads = sorted({measurement.number_of_threads for measurement in measurements_plot})

    keys = ['ro', 'bx', 'g^', 'cp', 'k>', 'y<']
    for i, degree in enumerate(broadcasting_degrees):
        values = [measurement.ms for measurement in measurements_plot if measurement.broadcast_degree == degree]
        plt.plot(numbers_of_threads, values, keys[i])
    plt.xticks(numbers_of_threads)
    plt.legend([str(x) for x in broadcasting_degrees], title=LEGEND_TITLE)

    plt.xlabel(NUMBER_OF_THREADS)
    plt.ylabel(TIME_NEEDED_PER_ACCESSES_MS)
    plt.title("Test " + text_test_mem_type(test, memory_type) + " memorije za uređaj " + DEVICE)
    plt.savefig("{0}{1}_{2}.png".format(output_filename, memory_type, test), bbox_inches="tight", format='png',
                dpi=1000)


def avg(list: List[float]):
    return sum(list) / len(list)


def diff_list(list1: List[float], list2: List[float]):
    a1 = avg(list1)
    a2 = avg(list2)
    return diff(a1, a2)


def diff(a1: float, a2: float):
    return min(a1, a2) / max(a1, a2)


def __is_ascending(list: List[float]) -> bool:
    for k in range(len(list) - 1):
        if not list[k] <= list[k + 1]:
            return False
    return True


def __is_descending(list: List[float]) -> bool:
    for k in range(len(list) - 1):
        if not list[k] >= list[k + 1]:
            return False
    return True


def broadcast_parallel(measurements: List[Measurement], memory_type: str):
    threads = sorted({measurement.number_of_threads for measurement in measurements})
    values_warp = [measurement.ms for measurement in measurements
                   if measurement.test == TEST1 and measurement.memory_type == memory_type
                   and measurement.number_of_threads == threads[0]]
    values_max = [measurement.ms for measurement in measurements
                  if measurement.test == TEST1 and measurement.memory_type == memory_type
                  and measurement.number_of_threads == threads[-1]]

    if __is_descending(values_warp):
        return "Broadcasting but not parallel accessing\n"
    elif __is_ascending(values_warp):
        return "Parallel accessing but not broadcasting\n"
    else:
        diff = abs(avg(values_warp) - avg(values_max))
        if diff < 0.15:  # 15% threshold
            return "Parallel accessing and broadcasting\n"
        else:
            return "Neither parallel accessing nor broadcasting\n"


def aligned_consecutive(measurements: List[Measurement], memory_type: str):
    threads_01 = {measurement.number_of_threads for measurement in measurements
                  if measurement.memory_type == memory_type and measurement.test == TEST1}
    threads_02 = {measurement.number_of_threads for measurement in measurements
                  if measurement.memory_type == memory_type and measurement.test == TEST2}
    threads_03 = {measurement.number_of_threads for measurement in measurements
                  if measurement.memory_type == memory_type and measurement.test == TEST3}
    threads = sorted(threads_01 & threads_02 & threads_03)
    values_01 = [measurement.ms for measurement in measurements
                 if measurement.test == TEST1 and measurement.memory_type == memory_type
                 and measurement.number_of_threads == threads[-1]]
    values_02 = [measurement.ms for measurement in measurements
                 if measurement.test == TEST2 and measurement.memory_type == memory_type
                 and measurement.number_of_threads == threads[-1]]
    values_03 = [measurement.ms for measurement in measurements
                 if measurement.test == TEST3 and measurement.memory_type == memory_type
                 and measurement.number_of_threads == threads[-1]]

    diff12 = diff_list(values_01, values_02)
    diff13 = diff_list(values_01, values_03)

    res = ""
    if abs(1 - diff12) > .15:
        res += "Aligned accessing is necessary\n"
    else:
        res += "Aligned accessing is not necessary\n"
    if abs(1 - diff13) > .15:
        res += "Consecutive accessing is necessary\n"
    else:
        res += "Consecutive accessing is not necessary\n"

    return res


if __name__ == '__main__':
    if not os.path.isfile(input_filename):
        print("No broadcast/parallel access file!")
    else:
        process_07_broadcast_parallel_access(input_filename)
