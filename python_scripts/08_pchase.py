import math
import os.path
from typing import List
import matplotlib.pyplot as plt

"""
A script processing P-chase and fine-grained P-chase microbenchmark results.
Returns:
    1) a set of graphs denoting the access time for all strides, varying by array size, for all memory types on the device
    2) a textual file with the algorithmic detection of fine-grained P-chase cache detection
"""

TLB = "TLB"
GLOBAL = "global"
LOCAL = "local"
CONSTANT = "constant"

english = False

AVERAGE_CLOCK_ACCESS = 'Average clock/access' if english else "Broj otkucaja po pristupu "
AVERAGE_ACCESS_TIME = "Average accesses/ms" if english else "Prosečna brzina pristupa {} (ms)"
BUFFER_SIZE_KIB = 'Buffer size (KiB)' if english else "Veličina bafera (KiB)"
BUFFER_SIZE_MIB = 'Buffer size (MiB)' if english else "Veličina bafera (MiB)"
TITLE = 'Cache detection for ' if english else "Detekcija keša na uređaju "

DEVICE = "AMD Radeon RX Vega 56"

input_filename_constant = "../immediate_results/08_pchase_constant.txt"
input_filename_local = "../immediate_results/08_pchase_local.txt"
input_filename_global = "../immediate_results/08_pchase_global.txt"
input_filename_TLB = "../immediate_results/08_pchase_TLB.txt"

input_filename_fg_global = "../immediate_results/08_fg_global.txt"
input_filename_fg_constant = "../immediate_results/08_fg_constant.txt"

output_folder = "../meaningful_results/r_08/"

output_filename_fg_global = "../meaningful_results/r_08/r_08_fg_global.txt"
output_filename_fg_constant = "../meaningful_results/r_08/r_08_fg_constant.txt"


def get_memory_type(memtype: str) -> str:
    memory_types = {GLOBAL: "global memory", LOCAL: "local memory", CONSTANT: "constant memory", "TLB": "TLB"} \
        if english else {GLOBAL: "globalnoj memoriji", LOCAL: "lokalnoj memoriji", CONSTANT: "konstantnoj memoriji",
                    "TLB": "TLB-u"}
    return memory_types[memtype]


class Measurement:
    # (OPERATION, TYPE, ITERATIONS, REPETITIONS, LATENCY(ms), OPERATION / ms, clk / OPERATION)
    def __init__(self, line: str):
        line = line.split('\t')
        line[0] = line[0][:line[0].find(" B")]
        self.size = int(line[0])
        self.stride = int(line[1])
        self.clk = float(line[2])

    def __repr__(self):
        return "({0}, {1}, {2})".format(self.size, self.stride, self.clk)


class FGMeasurement:
    # (size, stride, latencies[], accesses[])
    def __init__(self, line):
        line1 = line[0].split('\t')
        self.size = int(line1[0])
        self.stride = int(line1[1])
        self.latencies = [int(x) for x in line[1].split(" ")]
        self.accesses = [int(x) for x in line[2].split(" ")]

    def cache_miss_existing(self, cache_hit: int, difference: int) -> bool:
        for latency in self.latencies:
            if abs(latency - cache_hit) > difference:
                return True
        return False

    def num_of_cache_misses(self, cache_hit: int, difference: int) -> int:
        misses = 0
        for latency in self.latencies:
            if abs(latency - cache_hit) > difference:
                misses += 1
        return misses

    def __repr__(self):
        return "{}, {}\n{}\n{}".format(self.size, self.stride, self.latencies, self.accesses)


def __get_measurements(input_filename: str) -> List[Measurement]:
    return [Measurement(line.strip()) for line in open(input_filename, 'r')]


def plot_graph_global(measurements: List[Measurement], mem_type: str, strides=None):
    N = 100
    plt.clf()
    sizes = sorted({measurement.size for measurement in measurements})
    sizes = [sizes[i] for i in range(0, len(sizes), N)]
    if not strides:
        strides = sorted({measurement.stride for measurement in measurements})
    all_clks = []
    for stride in strides:
        v = [measurement.clk for measurement in measurements if measurement.stride == stride]
        v = [v[i] for i in range(0, len(v), N)]
        all_clks.append(v)

    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
              'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    markers = ['X', 'D', '<', '>', '^', 'v', 's', 'p', 'P', '*']

    plt.xlabel(BUFFER_SIZE_MIB)
    plt.ylabel(AVERAGE_ACCESS_TIME.format(get_memory_type(mem_type)))

    for i, clks in enumerate(all_clks):
        plt.plot(sizes, clks, color=colors[i], marker=markers[i], markevery=100)
    max_kb = math.ceil(sizes[-1] / (1024 * 1024))
    xticks = [i * 1024 * 1024 for i in range(max_kb + 1)]
    xticks = [xticks[i] for i in range(0, len(xticks))]
    xticks_labels = [int(xtick / (1024 * 1024)) for xtick in xticks]
    plt.xticks(ticks=xticks, labels=xticks_labels)
    plt.title(TITLE + DEVICE)
    plt.legend([str(x) for x in strides], title="Raskorak")
    plt.savefig(output_folder + "r_08_{}_pchase.png".format(mem_type), bbox_inches="tight", format='png', dpi=1000)


def plot_graph_TLB(measurements: List[Measurement], mem_type: str):
    N = 1000
    plt.clf()
    sizes = sorted({measurement.size for measurement in measurements})
    sizes = [sizes[i] for i in range(0, len(sizes), N)]
    strides = sorted({measurement.stride for measurement in measurements})
    all_clks = []
    for stride in strides:
        v = [measurement.clk for measurement in measurements if measurement.stride == stride]
        v = [v[i] for i in range(0, len(v), N)]
        all_clks.append(v)

    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
              'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    markers = ['X', 'D', '<', '>', '^', 'v', 's', 'p', 'P', '*']

    plt.xlabel(BUFFER_SIZE_MIB)
    plt.ylabel(AVERAGE_ACCESS_TIME.format(get_memory_type(mem_type)))

    for i, clks in enumerate(all_clks):
        plt.plot(sizes, clks, color=colors[i], marker=markers[i], markevery=100)
    max_kb = math.ceil(sizes[-1] / (1024 * 1024))
    xticks = [i * 1024 * 1024 for i in range(max_kb + 1)]
    xticks = [xticks[i] for i in range(0, len(xticks), 20)]
    xticks_labels = [int(xtick / (1024 * 1024)) for xtick in xticks]
    plt.xticks(ticks=xticks, labels=xticks_labels)
    plt.legend([str(x / 1024 / 1024) + " MiB" for x in strides], title="Raskorak")
    plt.plot(markers=markers, markevery=100)
    plt.title(TITLE + DEVICE)
    plt.savefig(output_folder + "r_08_{}_pchase.png".format(mem_type), bbox_inches="tight", format='png',
                dpi=1000)


def plot_graph_local(measurements: List[Measurement], mem_type: str):
    plt.clf()
    sizes = sorted({measurement.size for measurement in measurements})
    strides = sorted({measurement.stride for measurement in measurements})
    all_clks = []
    for stride in strides:
        v = [measurement.clk for measurement in measurements if measurement.stride == stride]
        all_clks.append(v)

    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
              'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    markers = ['X', 'D', '<', '>', '^', 'v', 's', 'p', 'P', '*']


    plt.xlabel(BUFFER_SIZE_KIB)
    plt.ylabel(AVERAGE_ACCESS_TIME.format(get_memory_type(mem_type)))

    for i, clks in enumerate(all_clks):
        plt.plot(sizes, clks, color=colors[i], marker=markers[i], markevery=100)
    max_kb = math.ceil(sizes[-1] / 1024)
    xticks = [i * 1024 for i in range(max_kb + 1)]
    xticks = [xticks[i] for i in range(0, len(xticks), 4)]
    xticks_labels = [int(xtick / 1024) for xtick in xticks]
    plt.xticks(ticks=xticks, labels=xticks_labels)
    plt.legend([str(x) for x in strides], title="Raskorak")
    # plt.ylim((350, 400))
    plt.plot(markers=markers, markevery=100)
    plt.title(TITLE + DEVICE)
    plt.savefig(output_folder + "r_08_{}_pchase.png".format(mem_type), bbox_inches="tight", format='png',
                dpi=1000)


def plot_graph_constant(measurements: List[Measurement], mem_type: str):
    plt.clf()
    sizes = sorted({measurement.size for measurement in measurements})
    strides = sorted({measurement.stride for measurement in measurements})
    all_clks = []
    for stride in strides:
        v = [measurement.clk for measurement in measurements if measurement.stride == stride]
        all_clks.append(v)

    colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple',
              'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
    markers = ['X', 'D', '<', '>', '^', 'v', 's', 'p', 'P', '*']

    plt.xlabel(BUFFER_SIZE_KIB)
    plt.ylabel(AVERAGE_ACCESS_TIME.format(get_memory_type(mem_type)))

    for i, clks in enumerate(all_clks):
        plt.plot(sizes, clks, color=colors[i], marker=markers[i], markevery=100)
    # xticks = [sizes[i] for i in range(0, len(sizes), 25)]
    max_kb = math.ceil(sizes[-1] / 1024)
    xticks = [i * 1024 for i in range(max_kb + 1)]
    xticks = [xticks[i] for i in range(0, len(xticks), 8)]
    xticks_labels = [int(xtick / 1024) for xtick in xticks]
    plt.xticks(ticks=xticks, labels=xticks_labels)
    plt.legend([str(x) for x in strides], title="Raskorak")
    plt.plot(markers=markers, markevery=100)
    plt.title(TITLE + DEVICE)
    plt.savefig(output_folder + "r_08_{}_pchase.png".format(mem_type), bbox_inches="tight", format='png', dpi=1000)


def plot_graph_strides():
    measurements = __get_measurements("../immediate_results/pchase_strides.txt")
    N = 1
    plt.clf()
    strides = [measurement.stride for measurement in measurements]
    timings = [measurement.clk for measurement in measurements]

    plt.xlabel("Veličina raskoraka")
    plt.ylabel("Broj otkucaja po pristupu globalnoj memoriji ")

    plt.plot(strides, timings, color='tab:blue')
    plt.title(TITLE + DEVICE)
    plt.savefig(output_folder + "r_08_pchase_STRIDE.png", bbox_inches="tight", format='png', dpi=1000)


def pchase():
    measurements_constant = __get_measurements(input_filename_constant)
    measurements_local = __get_measurements(input_filename_local)
    measurements_global = __get_measurements(input_filename_global)
    measurements_TLB = __get_measurements(input_filename_TLB)
    plot_graph_constant(measurements_constant, CONSTANT)
    plot_graph_local(measurements_local, LOCAL)
    plot_graph_global(measurements_global, GLOBAL)
    plot_graph_TLB(measurements_TLB, TLB)
    #plot_graph_strides()


def fine_grained_pchase(input_filename, output_filename, cache_hit: int, cache_diff: int):
    output = open(output_filename, "w")
    lines = [line.strip() for line in open(input_filename, "r").readlines()]
    print(lines)
    lines = [(lines[i].strip(), lines[i + 1].strip(), lines[i + 2].strip()) for i in range(0, len(lines), 3)]
    measurements = [FGMeasurement(line) for line in lines]
    measurements_one = [measurement for measurement in measurements if measurement.stride == 1]
    misses = [(measurement.size, measurement.num_of_cache_misses(cache_hit, cache_diff))
              for measurement in measurements_one]
    misses = [miss for miss in misses if miss[1] != 0]
    if misses:
        C = misses[0][0]
        output.write("A cache detected at {} B!\n".format(C))
        if len(misses) == 1:
            output.write("No other data available!")
            return
        else:
            num_misses = misses[0][0]
            for miss in misses[1:]:
                line_size = miss[0] - num_misses
                if line_size > 5:
                    output.write("Line size: {}!\n".format(line_size))
                    break
            else:
                output.write("Line size not detectable!")
            misses_ls = [(measurement.size, measurement.num_of_cache_misses(cache_hit, cache_diff))
                         for measurement in measurements if measurement.stride == line_size and measurement.size >= C
                         and (measurement.size % line_size) == 0]
            misses_ls = [miss for miss in misses_ls if miss[1] == misses_ls[-1][1]]
            N = misses_ls[0][0]
            T = (N - C) / line_size - 1
            output.write("Set associativity: {}".format(T))

    else:
        output.write("No cache detected!")


def process_08_pchase():
    pchase()
    fine_grained_pchase(input_filename_fg_global, output_filename_fg_global, 774, 50)
    fine_grained_pchase(input_filename_fg_constant, output_filename_fg_constant, 564, 50)


if __name__ == '__main__':
    if not os.path.isfile(input_filename_local):
        print("No latency and throughput file!")
    else:
        process_08_pchase()
