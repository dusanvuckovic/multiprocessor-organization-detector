import os

input_filename = "../immediate_results/05_simt_deadlock.txt"
output_filename = "../meaningful_results/r_05_deadlock.txt"

"""
A script processing the SIMT deadlock microbenchmark results.
Returns the number of threads which have deadlocked.
"""
def simt_deadlock(input_filename: str):
    if not os.path.isfile(input_filename):
        print("No input file at {}!".format(input_filename))
        return

    vals = [int(x) for x in open(input_filename).readlines()[0].strip().split(" ")]
    cnt = vals.count(0)
    output = open(output_filename, 'w')
    output.write("A total of {0} threads were deadlocked in the work-group.".format(cnt))


if __name__ == '__main__':
    simt_deadlock(input_filename)
