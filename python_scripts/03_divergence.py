import os
from collections import Counter

input_filename = "../immediate_results/03_divergence.txt"
output_filename = "../meaningful_results/r_03_divergence.txt"

"""
A script processing the divergence microbenchmark.
Returns a text file noting whether divergence was detected.
"""

def divergence(input_filename: str):
    if not os.path.isfile(input_filename):
        print("No input file at {}!".format(input_filename))
        return
    values = Counter([int(val) for val in open(input_filename).readlines()[0].split(' ') if val != ''])

    output = open(output_filename, mode='w')
    if len(values) == 1:
        output.write("No divergence detected.")
    else:
        output.write("Divergence detected.")


if __name__ == '__main__':
    divergence(input_filename)
