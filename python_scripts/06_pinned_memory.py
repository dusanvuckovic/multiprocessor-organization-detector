import os.path
from typing import List
import matplotlib.pyplot as plt
from matplotlib import rcParams

english = False

"""
A script processing the pinned memory microbenchmark results.
Returns a graph denoting the memory access time results.
"""

TIME_NEEDED_MS = 'Time needed (ms)' if english else "Vreme izvršavanja (ms)"
SIZE = 'Size (KiB)' if english else "Veličina bafera (KiB)"
DEVICE_TITLE = "Intel Core i7-6700"
TITLE = "Pinned memory" if english else "Zaključavanje stranica"
LEGEND_LABELS = ['pinned read', 'unpinned read', 'pinned write', 
                 'unpinned write', 'pinned readwrite', 'unpinned readwrite'] if english else \
                ['zaključano čitanje', 'ne-zaključano čitanje', 'zaključano pisanje',
                 'ne-zaključano pisanje', 'zaključano čitanje i pisanje', 'ne-zaključano čitanje i pisanje']

input_filename = "../immediate_results/06_pinned_v_unpinned.txt"
output_filename = "../meaningful_results/r_06_pinned_v_unpinned.png"


class Measurement:
    # (size, pn_read, pn_write, pn_readwrite, up_read, up_write, up_readwrite)
    def __init__(self, line: List[str]):
        self.size = int(line[0])
        self.pn_rd = float(line[1])
        self.pn_wr = float(line[2])
        self.pn_rw = float(line[3])
        self.up_rd = float(line[4])
        self.up_wr = float(line[5])
        self.up_rw = float(line[6])

    def __repr__(self):
        return "({0}, {1}, {2}, {3}, {4}, {5}, {6})".format(self.size, self.pn_rd, self.pn_wr,
                                                            self.pn_rw, self.up_rd,
                                                            self.up_wr, self.up_rw)


def __get_measurements(input_filename: str) -> List[Measurement]:
    f = open(input_filename, 'r')
    lines = f.readlines()
    lines = [line.rstrip()[1:-2] for line in lines]
    lines = [ln.split(', ') for ln in lines]
    measurements = [Measurement(line) for line in lines]
    return measurements


def process_06_pinned(input_filename: str):
    rcParams.update({'figure.autolayout': True})
    if not os.path.isfile(input_filename):
        print("No file in " + input_filename)
        return
    # A counter of pairs (clock_value, times_occurred).
    measurements = __get_measurements(input_filename)
    make_plot(measurements)


def make_plot(measurements: List[Measurement]):
    sizes = [measurement.size for measurement in measurements]
    pinned_read = [measurement.pn_rd for measurement in measurements]
    unpinned_read = [measurement.up_rd for measurement in measurements]
    pinned_write = [measurement.pn_wr for measurement in measurements]
    unpinned_write = [measurement.up_wr for measurement in measurements]
    pinned_readwrite = [measurement.pn_rw for measurement in measurements]
    unpinned_readwrite = [measurement.up_rw for measurement in measurements]
    ax = plt.gca()
    ax.set_xscale('log', basex=2)
    plt.plot(sizes, pinned_read, 'ro')
    plt.plot(sizes, unpinned_read, 'bx')
    plt.plot(sizes, pinned_write, 'g^')
    plt.plot(sizes, unpinned_write, 'cp')
    plt.plot(sizes, pinned_readwrite, 'k>')
    plt.plot(sizes, unpinned_readwrite, 'y<')
    plt.xlabel(SIZE)
    plt.ylabel(TIME_NEEDED_MS)
    plt.legend(LEGEND_LABELS)
    plt.xticks(sizes)
    plt.title(TITLE + ", " + DEVICE_TITLE)
    plt.savefig(output_filename, bbox_inches="tight", format='png', dpi=1000)


if __name__ == '__main__':
    if not os.path.isfile(input_filename):
        print("No pinned memory file!")
    else:
        process_06_pinned(input_filename)
